import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { LandingPageModule } from './landing-page/landing-page.module';
import { HttpClientModule } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { CoreModule } from './core/core.module';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { AppComponent } from './app.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap';
import { PatientSearchModule } from './patient-search/patient-search.module';
import { LoginModule } from './login/login.module';
import { PatientProfileModule } from './patient-profile/patient-profile.module';
import { PatientCreateModule } from './patient-create/patient-create.module';
import { AppointmentModule } from './appointment/appointment.module'
import { DirectivesModule } from './directives/directives.module';
import { PatientFlowSheetModule } from './patient-flow-sheet/patient-flow-sheet.module';
import { BookAppointmentModule } from './book-appointment/book-appointment.module';
import {AcceptcopayModule} from './accept-copay/accept-copay.module'
import { OfficeCalendarModule } from './office-calendar/officecalendar.module';

import { ToastrModule } from 'ng6-toastr-notifications';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LandingPageModule,
    PatientSearchModule,
    PatientProfileModule,
    AppointmentModule,
    PatientFlowSheetModule,
    LoginModule,
    PatientCreateModule,
    HttpClientModule,
    CoreModule,
    ButtonsModule,
    BrowserAnimationsModule,
    DirectivesModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    BookAppointmentModule,
    OfficeCalendarModule,
    AcceptcopayModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    ToastrModule.forRoot()
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
