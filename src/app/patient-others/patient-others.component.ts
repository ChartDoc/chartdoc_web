import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import {Alert, Allergies, Immunizations} from './patient-others-model';
import { SharedService } from 'src/app/core/shared.service';
import { PatientCreateService } from '../services/patient-create.service';

@Component({
  selector: 'app-patient-others',
  templateUrl: './patient-others.component.html',
  styleUrls: ['./patient-others.component.css']
})
export class PatientOthersComponent implements OnInit {

  allergyArray: Array<Allergies> = [];
  immunizationArray: Array<Immunizations> = [];
  alertKeywordArray=[];
  patientId ='0';
  ClsAllergyImmunization = {
    PatientID: '0',
    Allergies: [],
    Immunizations: [],
    Alert: {},
  }
  patient_FormGroup = new FormGroup({
    firstName: new FormControl(''),
    middleName:new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),
    DOB: new FormControl('')
  });

  allergies_FormGroup=new FormGroup({
    allergies_Description: new FormControl(''),
    allergies_Code: new FormControl(''),
  });

  alert_FormGroup=new FormGroup({
    alert_Description: new FormControl(''),
    alert_Code: new FormControl(''),
  });

  immunizations_FormGroup=new FormGroup({
    immunizations_Date: new FormControl(''),
    immunizations_Code: new FormControl(''),
    immunizations_Description: new FormControl(''),
  });

  constructor(private patientCreateService: PatientCreateService, private router: Router, private sharedService: SharedService) { }

  ngOnInit() {
    var patientDetail = this.sharedService.getLocalItem("patientDetail");
    if(patientDetail!==null){
      this.patientId =patientDetail.PatientId;
      this.patient_FormGroup.patchValue({
        firstName:patientDetail.FirstName,
        middleName:patientDetail.MiddleName,
        lastName:patientDetail.LastName,
        DOB:patientDetail.DOB,
        gender:patientDetail.Gender
      });

      this.patientCreateService.getImmunizations(this.patientId)
        .subscribe((res) => {
          res.forEach(element => {
          this.immunizationArray.push(
          {
            Code:element.Code,
            Date:element.Date,
            Description:element.Description,
            PatientId:element.PatientId,
            ID:element.ID
          });
        });
            //  console.log(this.immunizationArray);   
      },
      err => {
        console.log(err);
      });

        this.patientCreateService.getAllergies(this.patientId)
        .subscribe((res) => {
          res.forEach(element => {
          this.allergyArray.push(
          {
            Code:element.Code,
            Description:element.Description,
            PatientId:element.PatientId,
            ID:element.ID
          });
        });
            //  console.log(this.allergyArray);   
      },
      err => {
        console.log(err);
      });
    }
    this.getMasterData('8');
  }
  finish() {
    this.ClsAllergyImmunization.PatientID = this.patientId;
    this.ClsAllergyImmunization.Immunizations = this.getImmunizations();
    this.ClsAllergyImmunization.Allergies = this.getAllergies();
    this.ClsAllergyImmunization.Alert = this.getAlert();

    this.patientCreateService.patientOtherService(this.ClsAllergyImmunization)
      .subscribe
      (
          res => {
            this.sharedService.removeLocalStorage("patientDetail");
            this.router.navigate(['/patient-search'], { queryParams: { mode: 'edit-patient' } }); 
          }, 
          err => {console.log(err);}
      );
  }
  getAllergies() {
    return this.allergyArray;
  }
  getImmunizations(){
    return this.immunizationArray;
  }
  getAlert(){
    var alert: Alert = {
      ID:'0',
      PatientID: this.patientId,
      Code: this.alert_FormGroup.value["alert_Code"],
      Description: this.alert_FormGroup.value["alert_Description"]
    };
    return alert;
  }

  addAllergies(){
    var alergy: Allergies = {
      ID:'0',
      PatientId: this.patientId,
      Code: this.allergies_FormGroup.value["allergies_Code"],
      Description: this.allergies_FormGroup.value["allergies_Description"]
    };
    this.allergyArray.push(alergy);
  }
  deleteRow(index){
    this.allergyArray.splice(index,1);
  }

  addImmunizations(){
    var immunization: Immunizations = {
      ID:'0',
      PatientId: this.patientId,
      Date:this.immunizations_FormGroup.value["immunizations_Date"],
      Code: this.immunizations_FormGroup.value["immunizations_Code"],
      Description: this.immunizations_FormGroup.value["immunizations_Description"]
    };
    this.immunizationArray.push(immunization);
  }
  deleteImmunizationRow(index){
    this.immunizationArray.splice(index,1);
  }
  getMasterData(key){
    this.patientCreateService.getMasterData(key)
    .subscribe
    (
        res => {this.alertKeywordArray = res; }, 
        err => {console.log(err);}
    );
  }
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };     
    container.setViewMode('year');
   }
}
