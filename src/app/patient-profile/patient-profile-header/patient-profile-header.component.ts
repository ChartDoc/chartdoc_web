import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonService } from 'src/app/core/common.service';
import { Subscription } from 'rxjs';
import {PatientProfileHeaderService} from '../../services/patient-profile-header.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-patient-profile-header',
  templateUrl: './patient-profile-header.component.html',
  styleUrls: ['./patient-profile-header.component.css']
})
export class PatientProfileHeaderComponent implements OnInit {
  patientInfo: any;
  doctorInfo: any;
  
  // BPopened :boolean;
  // Pulseopened :boolean;
  // Heightopened:boolean;
  // Weightopened:boolean;
  // Tempopened:boolean;
  // Respiratoryopened:boolean;
  patientName:string;
  patientImage:string;
  patientAge: string;
  patientGender: string;

  BPFirst:string;
  BPLast:string;
  Pulse:string;
  Height1:string;
  Height2:string;
  weight:string;
  temprature:string;
  Respiratory:string;
  message: string;

  public windowTop: number = 150;
  public windowLeft: number = 10;
  closeResult: string;
  isView: boolean;
  doctorName: string;
  doctorImg: string;

  Commonsubscription: Subscription;
  Apisubscription: Subscription;
  PatientId:string;

  Flag: string;

  ObservationEntry: any;

  formObservation:FormGroup;

  constructor(private ProfileheaderService:PatientProfileHeaderService) { 
  }

  ngOnInit() {

    this.formObservation = new FormGroup({
      PatientId: new FormControl(""),
      pBloodPressure_L: new FormControl(""),
      pBloodPressure_R: new FormControl(""),
      pPulse: new FormControl(""),
      pHeight_L: new FormControl(""),
      pHeight_R: new FormControl(""),
      pWeight: new FormControl(""),
      pTemperature: new FormControl(""),
      pRespiratory: new FormControl("")
    });

    this.patientInfo = this.ProfileheaderService.getPatientDetails('patientInfo');
    this.doctorInfo = this.ProfileheaderService.getDoctorDetails('doctorInfo');

    this.doctorName = this.doctorInfo.doctorName;
    this.doctorImg = this.doctorInfo.doctorImage;

    this.PatientId = this.patientInfo.patientId;
    this.Flag = this.patientInfo.flag;
    this.patientName = this.patientInfo.patientFullName;
    this.patientImage = this.patientInfo.patientImagePath;
    this.patientAge = this.patientInfo.patientAge;
    this.patientGender = this.patientInfo.patientGender;

    this.Apisubscription = this.ProfileheaderService.GetPatientObservations(this.PatientId,this.Flag)
      .subscribe((res) => {
        this.ObservationEntry = res;
        this.BPFirst=res[0].pBloodPressure_L;
        this.BPLast=res[0].pBloodPressure_R;
        this.Pulse=res[0].pPulse;
        this.Height1=res[0].pHeight_L;
        this.Height2=res[0].pHeight_R;
        this.weight=res[0].pWeight;
        this.temprature=res[0].pTemperature;
        this.Respiratory=res[0].pRespiratory;
      },
      err => {
        console.log(err);
      });
  }

  viewDetails(){
    this.isView = true;
  }

  //--BP Section --//
  public closeBP(myModal: string) {
    document.getElementById(myModal).style.display = "none";
  }

  public openBP(myModal: string) {
    document.getElementById(myModal).style.display = "block";
  }

  public submitBP(BP1:string,BP2:string) {
    this.BPFirst=BP1;
    this.BPLast=BP2;

    this.ProfileheaderService.setBloodPressure('BP', this.BPFirst + '/' + this.BPLast);
    this.closeBP('modalBP');
  }
  //--BP Section --//

  //--Pulse Section --//
  public closePulse(myModal: string) {
    document.getElementById(myModal).style.display = "none";
  }

  public openPulse(myModal: string) {
    document.getElementById(myModal).style.display = "block";
  }

  public submitPulse(Pulse:string) {
      this.Pulse=Pulse;
      this.ProfileheaderService.setPulse('pulse', this.Pulse);
      this.closePulse('modalPulse');
  }
  //--Pulse Section --//

  //--Height Section --//
  public closeHeight(myModal: string) {
    document.getElementById(myModal).style.display = "none";
  }

  public openHeight(myModal: string) {
    document.getElementById(myModal).style.display = "block";
  }

  public submitHeight(Height1:string,Height2:string) {
    this.Height1=Height1;
    this.Height2=Height2;

    this.ProfileheaderService.setHeight('height', `${this.Height1}'${this.Height2}"`);
    this.closeHeight('modalHeight');
  }
  //--Height Section --//

  //--Weight Section --//
  public closeWeight(myModal: string) {
    document.getElementById(myModal).style.display = "none";
  }

  public openWeight(myModal: string) {
    document.getElementById(myModal).style.display = "block";
  }

  public submitWeight(Weight:string) {
    this.weight=Weight;
    this.ProfileheaderService.setWeight('weight',this.weight);
    this.closeWeight('modalWeight');
  }
  //--Weight Section --//

  //--Temprature Section --//
  public closeTemp(myModal: string) {
    document.getElementById(myModal).style.display = "none";
  }

  public openTemp(myModal: string) {
    document.getElementById(myModal).style.display = "block";
  }

  public submitTemp(temp:string) {
      this.temprature=temp;
      this.ProfileheaderService.setTemperature('temperature',this.temprature);
      this.closeTemp('modalTemp');
  }
  //--Temprature Section --//

  //--Respiratory Section --//
  public closeResp(myModal: string) {
    document.getElementById(myModal).style.display = "none";
  }

  public openResp(myModal: string) {
    document.getElementById(myModal).style.display = "block";
  }

  public submitResp(resp:string) {
    this.Respiratory=resp;
    this.ProfileheaderService.setRespiratory('respiratory', this.Respiratory);
    this.closeResp('modalResp');
  }

  //--Respiratory Section --//

  public Exit(){
    this.isView = false;
  }

  public saveObservation(){
    this.formObservation.setValue({
      PatientId: this.PatientId,
      pBloodPressure_L: this.BPFirst,
      pBloodPressure_R: this.BPLast,
      pPulse: this.Pulse,
      pHeight_L: this.Height1,
      pHeight_R: this.Height2,
      pWeight: this.weight,
      pTemperature: this.temprature,
      pRespiratory: this.Respiratory
    });

    this.ProfileheaderService.saveObservation(this.formObservation.value).subscribe
     (
       data=>
       {
         if(data > 0){
           this.message = 'Operation Successful';
         }
         else{
          this.message = 'Operation Unsuccessful';
         }
       }      
     )
  }

  //Numeric Validation
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
