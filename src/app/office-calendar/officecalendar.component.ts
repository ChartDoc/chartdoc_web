import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/user.model';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective } from '@angular/forms';
import { EventInput } from '@fullcalendar/core';
import { DatePipe } from '@angular/common';
import { AppointmentService } from '../services/appointment.service';
import { OfficeCalendarService } from '../services/officecalendar.service';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrManager } from 'ng6-toastr-notifications';
@Component({
    selector: 'office-calendar-appointment',
    templateUrl: './officecalendar.component.html',
    styleUrls: ['./officecalendar.component.css'],
    providers: [DatePipe]
})
export class OfficeCalendarComponent implements OnInit {
    formData = new FormData();
    @ViewChild('formDir', { static: false }) private formDirective: NgForm;
    officecalendarForm: FormGroup;
    doctors: any;
    calendardtl: any;
    today = new Date();
    minDate = undefined;
  
    constructor(private router: Router,
        private loginService: AuthenticationService,
        private formBuilder: FormBuilder,
        public datepipe: DatePipe,
        public _appointmentService: AppointmentService,
        public _officeCalendarService: OfficeCalendarService,
        public toastr: ToastrManager
    ) {
        const current = new Date();
        // this.minDate = {
        //     year: current.getFullYear(),
        //     month: current.getMonth() + 1,
        //     day: current.getDate()
        // };
        this.minDate = current;
        this.officecalendarForm = this.formBuilder.group({
            id: ['0'],
            tag: ['A', [Validators.required]],
            doctorId: ['0', [Validators.required]],
            calendardate: ['', [Validators.required]],
            fromTime: ['', [Validators.required]],
            toTime: ['', [Validators.required]],
            booingTag: ['1', [Validators.required]],
            eventReason: [''],
            date:['']
        });

    }

    ngOnInit() {
        let today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        let strdate = mm + '' + dd + '' + yyyy;

        this.getDoctorList(strdate);
        this.getAllServices();
        this.doctorselect() ;
    }
    timeValidation(): boolean {
        if (this.officecalendarForm.value.fromTime.hour <8 || this.officecalendarForm.value.fromTime.hour>=18) {
            this.toastr.errorToastr("From time must be between than 8 to 18", 'Oops!');
            return false;
        }
        if (this.officecalendarForm.value.toTime.hour <8 || this.officecalendarForm.value.toTime.hour>=18) {
            this.toastr.errorToastr("To time must be between than 8 to 18", 'Oops!');
            return false;
        }
        if (this.officecalendarForm.value.fromTime.hour > this.officecalendarForm.value.toTime.hour) {
            this.toastr.errorToastr("From time cannot be less than To time ", 'Oops!');
            return false;
        }
        else if (this.officecalendarForm.value.fromTime.hour == this.officecalendarForm.value.toTime.hour
            && this.officecalendarForm.value.fromTime.minute > this.officecalendarForm.value.toTime.minute) {
            this.toastr.errorToastr("From time cannot be less than To time ", 'Oops!');
            return false;
        }
        return true;
    }
    save() {
        let fromtime=this.officecalendarForm.value.fromTime;
        let toTime=this.officecalendarForm.value.toTime;
        if (!this.officecalendarForm.valid) {
            return;
        } else {
            if (this.timeValidation()) {
            //     this.officecalendarForm.patchValue({
            //         date:String(this.officecalendarForm.value.calendardate.getMonth() + 1).padStart(2, '0')+"/"+
            //         String(this.officecalendarForm.value.calendardate.getDate()).padStart(2, '0')+"/"+
            //         this.officecalendarForm.value.calendardate.getFullYear()
            // })
            this.officecalendarForm.value.date=String(this.officecalendarForm.value.calendardate.getMonth() + 1).padStart(2, '0')+"/"+
            String(this.officecalendarForm.value.calendardate.getDate()).padStart(2, '0')+"/"+
            this.officecalendarForm.value.calendardate.getFullYear();
            this.officecalendarForm.value.doctorId=this.officecalendarForm.controls['doctorId'].value 
                this.officecalendarForm.value.fromTime=String(this.officecalendarForm.value.fromTime.hour).padStart(2, '0')+":"+
                String(this.officecalendarForm.value.fromTime.minute).padStart(2, '0');
                this.officecalendarForm.value.toTime=String(this.officecalendarForm.value.toTime.hour).padStart(2, '0')+":"+
                String(this.officecalendarForm.value.toTime.minute).padStart(2, '0');
                console.log(this.officecalendarForm.value);

                
                this._officeCalendarService.saveCalendar(this.officecalendarForm.value)
                    .subscribe((res) => {
                        console.log('Response:%o', res);
                        this.formDirective.resetForm();
                        // this.officecalendarForm.reset();
                        this.officecalendarForm.patchValue({
                            tag: 'A',
                            doctorId: '0',
                            booingTag: '1',
                            date: '',
                            calendardate:'',
                            fromTime: '',
                            toTime: '',
                            eventReason: '',
                            id: 0
                        })
                        this.getAllServices();

                    }, err => {
                        console.log(err);
                        this.toastr.errorToastr(String(err) + " , please contact system admin!", 'Oops!');
                        this.officecalendarForm.value.fromTime=fromtime;
                        this.officecalendarForm.value.fromTime=toTime;
                    });
            }
        }
    }
    getDoctorList(strdate: any) {
        this._appointmentService.getDoctorList(strdate)
            .subscribe((res) => {
                this.doctors = res;

            },
                err => {
                    console.log(err);
                });
    }
    getAllServices() {
        this._officeCalendarService.getAllCalendar()
            .subscribe((res) => {
                this.calendardtl = res;

            }, err => {
                console.log(err);
            });
    }
    reset(): void {
        this.officecalendarForm.reset();
        this.officecalendarForm.patchValue({
            tag: 'A',
            doctorId: '0',
            booingTag: '1',
            id: 0
        })
    }
    rowclick(rowdata: any) {
        console.log(rowdata);
        let strtdate = rowdata.Date.substring(6, 10) + "" + rowdata.Date.substring(0, 2) + "" + rowdata.Date.substring(3, 5)
        let fromhr=rowdata.FromTime.split(":")[0];
        let frommmin=rowdata.FromTime.split(":")[1];
        let today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();
        let strdate1 = yyyy + '' + mm + '' + dd;
        if (Number(strtdate) >= Number(strdate1)) {
            this.officecalendarForm.patchValue({
                id: rowdata.Id,
                tag: rowdata.TAG,
                doctorId: rowdata.DoctorID,
                booingTag: rowdata.BooingTag==""?1:rowdata.BooingTag,
                calendardate:new Date(rowdata.Date.substring(6, 10) + "-" + rowdata.Date.substring(0, 2) + "-" + rowdata.Date.substring(3, 5)),
                date: rowdata.Date,
                fromTime:{hour:Number(fromhr),minute:Number(frommmin),second:0} ,//rowdata.FromTime.substring(0, 5),
                toTime:{hour:Number(rowdata.ToTime.split(":")[0]),minute:Number(rowdata.ToTime.split(":")[1]),second:0}, //rowdata.ToTime.substring(0, 5),
                eventReason: rowdata.EventReason,

            })
        } else {
            this.toastr.infoToastr("Creating & editing an event of previous date is not permitted" + " , please contact system admin!", 'Info!');
        }
    }
    doctorselect() {
        if (this.officecalendarForm.value.tag == "A") {
            this.officecalendarForm.controls['doctorId']!.disable();
        } else {
            this.officecalendarForm.controls['doctorId']!.enable();
        }
        this.officecalendarForm.patchValue({

            doctorId: '0'

        })
    }
}