import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { OfficeCalendarRoutingModule } from './officecalendar-routing.module'
import { OfficeCalendarComponent } from './officecalendar.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppointmentService } from '../services/appointment.service';
import { ShellModule } from '../shell/shell.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {OfficeCalendarService} from '../services/officecalendar.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
@NgModule({
  declarations: [OfficeCalendarComponent],
  imports: [
    CommonModule,
    ShellModule,
    OfficeCalendarRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot()
  ],
  providers:[AppointmentService,OfficeCalendarService],
  exports: [OfficeCalendarComponent]
})
export class OfficeCalendarModule { }
