import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { PatientdiagnosisService } from '../../services/patientdiagnosis.service';
import { PatientDiagnosis } from 'src/app/models/patient-diagnosis';
import { DiagnosisDocument } from 'src/app/models/diagnosis-document.model';
import { isNullOrUndefined } from 'util';


@Component({
  selector: 'app-patient-diagnosis',
  templateUrl: './patient-diagnosis.component.html',
  styleUrls: ['./patient-diagnosis.component.css']
})


export class PatientDiagnosisComponent implements OnInit,OnDestroy {

  DiagnosisData: any;
  DiagnosisEntry: Array<PatientDiagnosis> = [];
  // Diagnosis:PatientDiagnosis = new PatientDiagnosis();
  Diagnosys: string[];
  diagnosisDocument: Array<DiagnosisDocument> = [];
  patientInfo: any;
  
  Commonsubscription: Subscription;
  Apisubscription: Subscription;

  PatientId:string;
  flag: string;
  pipe = new DatePipe('en-US'); // Use your own locale


  constructor(private DiagnosisService:PatientdiagnosisService) { }

  ngOnDestroy(){
    // this.Commonsubscription.unsubscribe();
    // this.Apisubscription.unsubscribe();
  }

  ngOnInit() {

    this.patientInfo = this.DiagnosisService.getPatientDetails('patientInfo');
    this.flag = this.patientInfo.flag;
    this.PatientId = this.patientInfo.patientId;
    
    if(isNullOrUndefined(this.DiagnosisService.getDiagnosisDetails('diagnosis' + this.PatientId))){
      this.Apisubscription = this.DiagnosisService.GetPatientDiagnosis(this.PatientId, this.flag)
      .subscribe((res) => {   
        if(this.flag == 'E'){
          res.entry.forEach(element => {          
          
            this.DiagnosisEntry.push(
              { 
                effectiveDateTime: this.pipe.transform(new Date(element.resource.effectiveDateTime), 'MMM dd,yyyy'), 
                Diagnosis: element.resource.code.text
              });
          });
        }        
        else{
          res.forEach(item => {
            this.DiagnosisEntry.push(
              {
                effectiveDateTime: this.pipe.transform(new Date(item.DiagnosisDate), 'MMM dd,yyyy'), 
                Diagnosis: item.DiagnosisDesc
              }
            )
          });
        }
        this.DiagnosisService.setDiagnosisDetails('diagnosis' + this.PatientId, this.DiagnosisEntry);
      },
      err => {
        console.log(err);
      });
    }
    else{
      this.DiagnosisService.getDiagnosisDetails('diagnosis' + this.PatientId)
    }
    
    this.getDocuments();
  }

  LoadDiagnosisDocuments(DiagDate:Date,Diagnosis:string){
    //alert('Loading documents for Date:'+DiagDate+' | Name:'+Diagnosis);
  }

  getDocuments(){
    if(isNullOrUndefined(this.DiagnosisService.getDocumentDetails('diagnosis-document' + this.PatientId))){
      this.DiagnosisService.GetDocumentByPatientId(this.PatientId, this.flag).subscribe((data: DiagnosisDocument[]) => {
        if(this.flag == 'E'){
          this.diagnosisDocument.push({
            patientId: this.PatientId,
            FileName: 'Jason Argonaut.pdf',
            Path: '../../../../assets/DiagnosisDocuments/Jason Argonaut.pdf'
          });
        }
        else{
          this.diagnosisDocument = data;          
        }
        this.DiagnosisService.setDocumentDetails('diagnosis-document' + this.PatientId, this.diagnosisDocument);
      });     
    }
    else{
      this.DiagnosisService.getDocumentDetails('diagnosis-document' + this.PatientId)
    }      
  }

  downloadFile(){
    this.diagnosisDocument.forEach(item => {
      let link = document.createElement("a");
      link.download = item.FileName;
      link.href = item.Path;
      link.click();
      link.remove();
    });
  }
}
