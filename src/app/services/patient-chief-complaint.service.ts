import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from '../core/shared.service';
import { environment } from 'src/environments/environment';

const localBASE_URL="api/ChartDoc/";
const epiclBASE_URL="api/ChartDoc";

@Injectable({
  providedIn: 'root'
})
export class PatientChiefComplaintService {

  constructor(private _http: HttpClient,
              private sharedService: SharedService) { }

  public GetPatientCC(patientId: string,flag:string): Observable<any>{

    const endpoint = '/GetChiefComplaint/';
    
    

    let myHeaders = new HttpHeaders(); 
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('Accept', 'text/plain');  
    
    if(flag==='L'){
           patientId  = btoa(patientId);
          return this._http.get(environment.baseUrl + localBASE_URL + `GetChiefComplaint/${patientId}`, { headers: myHeaders});
    }
    else { // require to use epic url
          patientId  = btoa(patientId);
          return this._http.get(environment.baseUrl + localBASE_URL + `GetChiefComplaint/${patientId}`, { headers: myHeaders});
    }
  }

  public saveCC(ccInfo: any): Observable<any>{
    return this._http.post(environment.baseUrl + localBASE_URL + `SaveChiefComplaint`, ccInfo);
  }

  setChiefComplaintValue(key: string, val: string){
    this.sharedService.setLocalItem(key, val);
  }

  getChiefComplaintValue(key: string): string{
    return this.sharedService.getLocalItem(key);
  }

  getPatientDetails(patientInfo: string){
    return this.sharedService.getLocalItem(patientInfo);
  }
}
