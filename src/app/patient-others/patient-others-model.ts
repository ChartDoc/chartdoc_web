
export class Alert {
    ID: string;
    PatientID : string;
    Code: string;
    Description: string;
}

export class Allergies
{
	ID: string;
	PatientId: string;
	Code: string;
    Description: string;
}

export class Immunizations
{
	ID: string;
	PatientId: string;
	Code: string;
	Description: string;
	Date: string;
}