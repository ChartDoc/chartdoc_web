import { Injectable } from '@angular/core';
import { SharedService } from '../core/shared.service';
import { DoctorInformation } from '../models/doctor-information';
import { Patient } from '../models/patient.model';

@Injectable({
  providedIn: 'root'
})
export class LandingPageService {

  constructor(private sharedService: SharedService) { }

  setDoctorInfo(key: string, val: any){
    this.sharedService.setLocalItem(key,val);
  }
}
