import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from '../core/shared.service';
import { environment } from 'src/environments/environment';


const SAVE_CALENDAR = "api/ChartDoc/SAVECALENDER";

const VIEW_CALENDAR = "api/ChartDoc/GETCALENDER";

@Injectable({
  providedIn: 'root'
})
export class OfficeCalendarService {

  constructor(private http: HttpClient, private sharedService: SharedService) { }
  
  
  public saveCalendar(request:object):Observable<any>{ 
    
    
    return this.http.post(environment.baseUrl +SAVE_CALENDAR,request);
   
  }
  public getAllCalendar():Observable<any>{    
    return this.http.get(environment.baseUrl + VIEW_CALENDAR);
  }
}
