import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const SAVE_PATIENT = "api/ChartDoc/CreatePatient";
const SAVE_PATIENTOTHERS = "api/ChartDoc/SaveAllergyImmunization";
const GET_PATIENTINFO = "api/ChartDoc/GetPatientInfo";
const GET_MASTERDATA = "api/ChartDoc/OtherPopulate";
const Get_IMMUNIZATIONS = "api/ChartDoc/GetImmunizations";
const GETALLERGIES = "api/ChartDoc/GetAllergies";
const FILE_UPLOAD = "api/ChartDoc/FileUpload";
@Injectable({
  providedIn: 'root'
})
export class PatientCreateService {
  constructor(private http: HttpClient) { }

  public savePatient(request: object): Observable<any> {
     var apiURL=environment.baseUrl + SAVE_PATIENT;
    //var apiURL="http://localhost:14403/" + SAVE_PATIENT;
    return this.http.post(apiURL, request);
  }

  public getPatientInfoByPatientId(patientId: string): Observable<any> {
    var apiURL=environment.baseUrl + GET_PATIENTINFO;
    //var apiURL="http://localhost:14403/" + GET_PATIENTINFO;
    return this.http.get(apiURL + `/${patientId}`);
  }

  public getMasterData(key: string): Observable<any> {
    var apiURL=environment.baseUrl + GET_MASTERDATA;
    //var apiURL="http://localhost:14403/" + GET_PATIENTINFO;
    return this.http.get(apiURL + `/${key}`);
  }

  public patientOtherService(request: object): Observable<any> {
    var apiURL=environment.baseUrl + SAVE_PATIENTOTHERS;
   //var apiURL="http://localhost:14403/" + SAVE_PATIENT;
   return this.http.post(apiURL, request);
 }
 public getImmunizations(PatientId: string): Observable<any> {
  var apiURL=environment.baseUrl + Get_IMMUNIZATIONS;
  //var apiURL="http://localhost:14403/" + Get_IMMUNIZATIONS;
  PatientId = btoa(PatientId);
  return this.http.get(apiURL + `/${PatientId}`);
}
public getAllergies(PatientId: string): Observable<any> {
  var apiURL=environment.baseUrl + GETALLERGIES;
  PatientId = btoa(PatientId);
  //var apiURL="http://localhost:14403/" + GETALLERGIES;
  return this.http.get(apiURL + `/${PatientId}`);
}

 fileUpload(formData: object) {
  //var apiURL=environment.baseUrl + FILE_UPLOAD;
   var apiURL="http://localhost:14403/" + FILE_UPLOAD;
   return this.http.post<any>(apiURL, formData);
}
}
