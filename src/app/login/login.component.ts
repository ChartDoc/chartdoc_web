import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/user.model';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isNew: boolean;
  doctorName: string;
  doctorImg: string;
  errorMsg: string="";
  userName: string;
  password: string;
  errorCode: string;
  errorDesc: string;
  doctorInfo: FormGroup;

  constructor(private router: Router, 
              private loginService: AuthenticationService,
              private formBuilder: FormBuilder) { 
    document.body.className = "login_page";
  }

  ngOnInit() {
    this.errorMsg = "";
    this.doctorInfo = this.formBuilder.group({
      doctorName: '',
      doctorImage: '',
      doctorId: 0
    });
    this.doctorName = this.loginService.getLocalStorage('doctorInfo').doctorName;
    this.doctorImg = this.loginService.getLocalStorage('doctorInfo').doctorImage;
    if(this.doctorName == ""){
      this.isNew=true;
    }
    else{
      this.isNew = false;
    }
  }

  ngOnDestroy(){
    document.body.className="sidebar-collapse";
  }

  tryLogin(){
    if(this.userName == undefined){
      this.userName = this.doctorName;
    }
    this.loginService.getLoginCredentials(this.userName, this.password)
      .subscribe((res:User) => {
        this.errorCode = res.ErrorCode;
        this.errorDesc = res.ErrorDesc;
               
        if(this.errorCode == "0"){
          this.doctorInfo.setValue({
            doctorName: this.doctorName,
            doctorImage: this.doctorImg,
            doctorId: res.IUserID
          }); 
          this.loginService.setDoctorInformation('doctorInfo', this.doctorInfo.value);
          this.router.navigateByUrl('/patient-search');
        }
        else{
          this.errorMsg = this.errorDesc;
        }
      });    
  }  

  // get token(){
  //   let claims: any = this.oauthService.getIdentityClaims();
  //   return claims ? claims : null;
  // }
}
