import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const BASE_URL = "api/chartdoc/GetFlowsheet/";
const SAVE_FlowSHEET = "api/chartdoc/UpdateFloorArea/";
const UPDATE_MARKIN_STATUS="api/chartdoc/UpdateMarkReady/"

@Injectable({
  providedIn: 'root'
})
export class PatientFlowSheetService {

  constructor(private http: HttpClient) { }

  public getAppointmentDetail(date: string):Observable<any>{    
    return this.http.get(environment.baseUrl + BASE_URL + date)
  }
  public updateAppoinmentDetail(AppointmentID:string,RoomNO:string,Flowarea:string):Observable<any>
  {
    return this.http.get(environment.baseUrl + SAVE_FlowSHEET+AppointmentID+"/"+RoomNO+"/"+Flowarea)
  }
  public updateMarkStatus(AppointmentID:string,Status:boolean):Observable<any>
  {
    
    return this.http.get(environment.baseUrl + UPDATE_MARKIN_STATUS+AppointmentID +"/"+ Status)

  }
}
