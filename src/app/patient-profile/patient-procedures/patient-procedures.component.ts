import { Component, OnInit, OnDestroy } from '@angular/core';
import {PatientProceduresService} from '../../services/patient-procedures.service'
import { Subscription } from 'rxjs';
import { PatientProcedures } from 'src/app/models/patient-procedures';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-patient-procedures',
  templateUrl: './patient-procedures.component.html',
  styleUrls: ['./patient-procedures.component.css']
})
export class PatientProceduresComponent implements OnInit,OnDestroy {
  data: any;
  isViewByDate:Boolean = true;
  Commonsubscription: Subscription;
  Apisubscription: Subscription;
  ProceduresEntry: Array<PatientProcedures> = [];
  PatientId:string;
  pipe = new DatePipe('en-US'); // Use your own locale
  value:number = 0;
  flag: string;

  constructor(private ProcedureService:PatientProceduresService) { }

  ngOnDestroy(){
    //this.Commonsubscription.unsubscribe();
    //this.Apisubscription.unsubscribe();
  }

  ngOnInit() {
    this.PatientId = this.ProcedureService.getPatientDetails('patientInfo').patientId;
    this.flag = this.ProcedureService.getPatientDetails('patientInfo').flag;
        
    this.Apisubscription = this.ProcedureService.GetPatientProcedures(this.PatientId, this.flag)
      .subscribe((res) => {
        if(this.flag == 'E'){
          res.entry.forEach(element => {
            this.ProceduresEntry.push(
              { 
                performedDateTime: this.pipe.transform(new Date(element.resource.performedDateTime), 'MMM dd,yyyy'), 
                ProcedureName: element.resource.code.text,
                DoctorName:this.value==0?'Dr.June Abigail':(this.value == 1? 'Dr. Benjamin Dartmoth':(this.value == 2?'Dr. Sanjay Gupta':(this.value==3?'Dr. Susan Mendenhall':'Dr. Michael Svenson'))),
                DoctorProfile:this.value==0?'Oncologist':(this.value==1?'Cardiologist':(this.value==2?'Gastroentologist':(this.value==3?'Primary Care Physician':'Cardiologist')))
            });
            this.value++;
          });
        }else{
          res.forEach(item => {
            this.ProceduresEntry.push(
              { 
                performedDateTime: this.pipe.transform(new Date(item.ProcedureDate), 'MMM dd,yyyy'), 
                ProcedureName: item.ProcedureDesc,
                DoctorName:item.Dr_Name,
                DoctorProfile:item.Dr_Profile
            });
            this.value++;
          });
        }
        
      },
      err => {
        console.log(err);
      });
  }

  ChangeProcedureView(value:string){
    if(value=='DATE')
    this.isViewByDate = true;
    else
    this.isViewByDate=false;
  }
  LoadProcedureDocumentsbyDate(PerformDate:Date,procedure:string){
    //alert('Loading documents for : Date:'+PerformDate+' | Procedure: '+procedure);
  }
  LoadProcedureDocumentsByDoctor(DoctorName:string,DoctorProfile:string){
    //alert('Loading documents for : Doc Name:'+DoctorName+' | Doc Profile: '+DoctorProfile);
  }

}
