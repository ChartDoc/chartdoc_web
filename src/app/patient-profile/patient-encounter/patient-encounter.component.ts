import { Component, OnInit, OnDestroy } from '@angular/core';
import {PatientEncounterService} from '../../services/patient-encounter.service';
import { Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { encodeBase64 } from '@progress/kendo-file-saver';
import { Encountermodel } from 'src/app/models/encountermodel';


@Component({
  selector: 'app-patient-encounter',
  templateUrl: './patient-encounter.component.html',
  styleUrls: ['./patient-encounter.component.css']
})
export class PatientEncounterComponent implements OnInit ,OnDestroy {
  Encounters: string[];
  formEncounter:FormGroup;
  public searchString: string;
  encounNotes:string ="";
  patientInfo: any;
  doctorId: number;

  PatientId:string;  
  Commonsubscription: Subscription;
  Apisubscription: Subscription;

  encModel:Array<Encountermodel> = [];
  pipe = new DatePipe('en-US'); // Use your own locale

  EncounterSummary:string = '';

  constructor(private encounterService:PatientEncounterService) {

   }

   ngOnDestroy(){
    //this.Commonsubscription.unsubscribe();
    //this.Apisubscription.unsubscribe();
  }

  ngOnInit() {
    this.encModel = []; 

    this.patientInfo = this.encounterService.getPatientDetails('patientInfo');
    this.PatientId = this.patientInfo.patientId;
    this.doctorId = this.encounterService.getDoctorDetails('doctorInfo').doctorId;
                                    
    this.formEncounter = new FormGroup({
      EncounterNote: new FormControl("")  ,
      Summary:new FormControl(""),
      PatientId:new FormControl(""),
      doctorId: new FormControl("")  
    });    

    this.loadEncounters();

    this.formEncounter.get("EncounterNote").setValue(this.encounterService.getEncounterNote('encounterNote' + this.doctorId + this.PatientId));
  }

  UsePreviousNote(Notes: string,summary:string){
    this.EncounterSummary = summary;
    this.formEncounter.get("EncounterNote").setValue(summary+"\n\n"+Notes);
    this.encounterService.setEncounterNote('encounterNote' + this.doctorId + this.PatientId, Notes)
    this.encounNotes = summary+"\n\n"+Notes;
  }

  submitEncounterSummary(EncounterSummary: string,myModal: string){    
    document.getElementById(myModal).style.display = "none";   

      this.formEncounter.setValue({
        EncounterNote: this.encounterService.getEncounterNote('encounterNote' + this.doctorId +this.PatientId),
        Summary:EncounterSummary,
        PatientId:this.PatientId,
        doctorId: this.doctorId
      } 
    )
    
    this.encounterService.saveEncounter(this.formEncounter.value).subscribe
    (
      data=>
      {
        this.encounterService.removeLocalEncounter('encounterNote' + this.doctorId + this.PatientId);
        this.encModel = []; 
        this.loadEncounters();  
        this.formEncounter.get("EncounterNote").setValue("");
      }      
    ) 

    this.EncounterSummary = '';    
    
  
    
  }

  UpdateEncounterNotes(value:string){
    this.encounterService.setEncounterNote('encounterNote' + this.doctorId + this.PatientId, value);
  }

  public OpenEncounterSummary(myModal: string) {
    // this.EncounterSummary
    document.getElementById(myModal).style.display = "block";
  }

  public closeEncounterSummary(myModal: string) {
    document.getElementById(myModal).style.display = "none";
  }

loadEncounters(){  
    this.Apisubscription = this.encounterService.GetPatientEncounters(encodeBase64(this.PatientId))
    .subscribe((res) => {
      res.forEach(element => {
      this.encModel.push(
      {
        EncounterDate: this.pipe.transform(new Date(element.EncounterDate), 'MMM dd,yyyy'),
        Summary:element.Summary,
        Name:element.Name,
        EncounterNote:element.EncounterNote,
        ID:element.ID
      });
    });
            
  },
  err => {
    console.log(err);
  });
}

}
