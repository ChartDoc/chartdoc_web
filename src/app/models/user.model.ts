export class User{
    DepartmentID: string;
    Department: string;
    Code: string;
    ErrorDesc: string;
    ErrorCode: string;
    UserID: string;
    UserType: string;
    APPLICABLETO: string;
    FNAME: string;
    UTNAME: string;
    UserName: string;
    IUserID: string;
    USERTAG: string;
}