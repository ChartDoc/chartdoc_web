import { Component, OnInit, OnDestroy } from '@angular/core';
import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { Subscription } from 'rxjs';
import { PatientImpressionPlanService } from 'src/app/services/patient-impression-plan.service';
import { FormGroup, FormControl } from '@angular/forms';
import { SharedService } from 'src/app/core/shared.service';
import { isNullOrEmptyString } from '@progress/kendo-angular-grid/dist/es2015/utils';

@Component({
  selector: 'app-patient-impression-plan',
  templateUrl: './patient-impression-plan.component.html',
  styleUrls: ['./patient-impression-plan.component.css']
})
export class PatientImpressionPlanComponent implements OnInit, OnDestroy {

  public Editor = DecoupledEditor;
  public ckeditorContent: string;
  tempckeditorContent: string;
  flag: string;
  dataDefault: any;
  PatientId: string;
  formImpressionPlan: FormGroup;

  Commonsubscription: Subscription;
  // Apisubscription: Subscription;

  constructor(private sharedService: SharedService,
              private patientImpressionPlanService: PatientImpressionPlanService) {
    this.ckeditorContent = '';
   }

   ngOnDestroy() {
    // this.Commonsubscription.unsubscribe();
  }


  ngOnInit() {
    this.PatientId = this.sharedService.getLocalItem('patientInfo').patientId;
    this.flag = this.sharedService.getLocalItem('patientInfo').flag;
    if (!isNullOrEmptyString(this.sharedService.getLocalItem('impressionPlan' + this.PatientId))) {
      this.ckeditorContent = this.sharedService.getLocalItem('impressionPlan' + this.PatientId);
    }
    else{
      this.patientImpressionPlanService.getPatientImpressionplan(this.PatientId, this.flag)
        .subscribe((res) => {
          this.ckeditorContent = res[0].Description;
        });
    }

    this.formImpressionPlan = new FormGroup({
      patientId: new FormControl(),
      description: new FormControl()
    });

  }

  public onReady( editor: any ) {
    //this.ckeditorContent = '';
    editor.ui.getEditableElement().parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement(),
    );

  }
  public onChange( { editor }: ChangeEvent ) {
    const data = editor.getData();
    this.sharedService.setLocalItem('impressionPlan' + this.PatientId, data);
  }
  public saveImpressionPlan() {

       this.formImpressionPlan.setValue({
       description: this.ckeditorContent,
       patientId: this.PatientId
     });

       this.patientImpressionPlanService.saveImpressionPlan(this.formImpressionPlan.value).subscribe
     (
       data => {
         // this.CCService.removeLocalCC('cc'+this.PatientId);
         // this.ckeditorContent = "";
       }
     );
   }
 }
