import { Component, OnInit } from '@angular/core';
import { PatientIcdService } from '../../services/patient-icd.service';
import { PatientICDModelEmployee } from '../../models/PatientICD.model';
import { FormGroup, FormControl } from '@angular/forms';
import { SharedService } from 'src/app/core/shared.service';


const form = new FormGroup({
  id: new FormControl(),
  Code: new FormControl(),
  Desc: new FormControl()
});

@Component({
  selector: 'app-patient-icd',
  templateUrl: './patient-icd.component.html',
  styleUrls: ['./patient-icd.component.css']
})
export class PatientIcdComponent implements OnInit {
  icdDetailss: PatientICDModelEmployee[]; // = [];
  icdid: number;
  icdCode: string;
  icdDesc: string;
  icdDetails: Array<PatientICDModelEmployee> = [];
  public searchTerm: string;
  public admin = false;
  public tblvisibility = false;
  public isvisibility = false;
  isIdExist: boolean;
  formICD: FormGroup;
  patientInfo: [] = [];
  PatientId: string;
  frm: PatientICDModelEmployee[] = [];
  icdGetSavedDetails: any;
  data: string;
  message: string;

  constructor(private patientIcdService: PatientIcdService) {
  }

  ngOnInit() {
    this.PatientId = this.patientIcdService.getPatientDetails('patientInfo').patientId;
    if(!this.patientIcdService.getIcdDetails('icd' + this.PatientId)){
      this.patientIcdService.getPatientIcdDetails()
        .subscribe((res) => {
          this.icdDetailss = res;
          // this.data = this.patientIcdService.getDynamicHtml(res);
          this.patientIcdService.setIcdDetails('icd' + this.PatientId, this.icdDetailss);
        },
        err => {
          console.log(err);
      });
    }
    else{
      this.icdDetailss = this.patientIcdService.getIcdDetails('icd' + this.PatientId);
    }
    
    this.tblvisibility = true;

    this.formICD = new FormGroup({
      ID: new FormControl(),
      patientId: new FormControl(),
      Code: new FormControl(),
      Desc: new FormControl()
    });

    this.patientIcdService.getSavedICD(this.PatientId)
    .subscribe((data) => {
      this.icdDetails = data;
    },
    err => {
      console.log(err);
    });
  }

  searchText(value: string){
    this.icdDetailss.filter(element => {
      element.Code.toLowerCase() === value.toLowerCase() || element.Desc.toLowerCase() === value.toLowerCase();
    });
    this.data = this.patientIcdService.getDynamicHtml(this.icdDetailss);
  }

  AddIcdDetails(icdId: string, icdCode: string, icdDesc: string) {
    this.tblvisibility = true;
    form.patchValue({id: icdId, Code: icdCode, Desc: icdDesc});
    this.isIdExist = false;
    this.icdDetails.forEach(element => {
      if (element.id === icdId) {
        this.isIdExist = true;
      }
    });

    if (!this.isIdExist) {
        this.icdDetails.push(form.value);
    }
  }

  deleteRow(icd: PatientICDModelEmployee) {
    const index: number = this.icdDetails.indexOf(icd);
    this.icdDetails.splice(index, 1);
    if (this.icdDetails.length < 1) {
      this.tblvisibility = false;
    }
  }

  public saveIcdDetails(e: Event) {
    e.preventDefault();
    this.icdDetails.forEach(item => {
      this.formICD.patchValue({
        Code: item.Code,
        Desc: item.Desc,
        patientId: this.PatientId,
        ID: item.id
      });
      this.frm.push(this.formICD.value);
    });

    this.patientIcdService.saveIcdDetails(this.frm)
    .subscribe(
      data => {
        if(data > 0){
          this.message = "Operation Successful";
        }
        else{
          this.message = "Operation Unsuccessful";
        }
      }
    );
  }
}
