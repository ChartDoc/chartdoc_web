import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput } from '@fullcalendar/core';
import { PatientFlowSheetService } from '../services/patient-flow-sheet.service';
import timeGrigPlugin from '@fullcalendar/timegrid';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction'; // for dateClick
import resourceTimeGridPlugin from '@fullcalendar/resource-timegrid';
import { Router } from '@angular/router';
import { parse } from 'querystring';
import { AppointmentService } from '../services/appointment.service';

@Component({
  selector: 'app-patient-flow-sheet',
  templateUrl: './patient-flow-sheet.component.html',
  styleUrls: ['./patient-flow-sheet.component.css']
})
export class PatientFlowSheetComponent implements OnInit {
  @ViewChild(FullCalendarComponent, { static: false }) calendarComponent: any; // the #calendar in the template
  // @ViewChild('external', { static: false }) external: ElementRef;
  calendarVisible = true;
  //calendarPlugins = [timeGrigPlugin, interactionPlugin, resourceTimeGridPlugin];
  calendarEvents: EventInput[] = [];
  calendarresources: EventInput[] = [];
  appoinmentId: string;
  roomNumber: string;
  FlowArea: string;
  defaultDate: any;
  markStatus: boolean;
  error: boolean;
  currentDate: string;

  //tempdata = [];
  calendarPlugins = [timeGrigPlugin, interactionPlugin, resourceTimeGridPlugin];
  patientData: [];


  constructor(private _patientFlowSheetService: PatientFlowSheetService, private _appointmentService: AppointmentService,
    private router: Router) {
    // this.markStatus=false;
  }

  getAppointmentDetails(strDate: any) {
    //alert("start date="+strDate);
    this._patientFlowSheetService.getAppointmentDetail(strDate)
      .subscribe((res) => {
        this.patientData = res;
        console.log("data=" + JSON.stringify(this.patientData))
        let tempdata = [];

        let calendarApi = this.calendarComponent.getApi();
        let nextdaydate = new Date(calendarApi.getDate())
        var dd = String(nextdaydate.getDate()).padStart(2, '0');
        var mm = String(nextdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = nextdaydate.getFullYear();
        let strdate1 = yyyy + '-' + mm + '-' + dd;
        this.defaultDate = yyyy + '-' + mm + '-' + dd

        res.forEach((element: any) => {
          if (element.schduleAppoinment.DoctorID != "") {
            tempdata.push({
              resourceId: 1, title: element.schduleAppoinment.PatientName.trim(), start: strdate1 + "T" + element.schduleAppoinment.FromTime.trim(),
              end: strdate1 + "T" + element.schduleAppoinment.ToTime.trim(), IsReady: element.schduleAppoinment.IsReady, appoinmentId: element.schduleAppoinment.AppointmentId, patientID: element.schduleAppoinment.PatientID,
              email: element.schduleAppoinment.Email, phone: element.schduleAppoinment.Phoneno,patientName:element.schduleAppoinment.PatientName,doctorName:element.schduleAppoinment.DoctorName
              , dateOfBirth: element.schduleAppoinment.DateofBirth, gender: element.schduleAppoinment.Gender, address: element.schduleAppoinment.Address,serviceId:element.schduleAppoinment.ServiceID,note:element.schduleAppoinment.Note,reason:element.schduleAppoinment.Reason,positionID:element.schduleAppoinment.PositionID,reasonID:element.schduleAppoinment.ReasonID
              ,color: element.schduleAppoinment.IsReady == "True" ? 'yellow' : element.schduleAppoinment.ColorCode.trim(), HeaderId: 1,appointmentFromTime:strdate1 + "T"+element.schduleAppoinment.AppointmentFromTime.trim(),appointmentToTime:strdate1 + "T"+element.schduleAppoinment.AppointmentToTime.trim(),roomNo:element.schduleAppoinment.ROOMNO
              // editable:
            });
          }
          else if (element.WaitingArea.DoctorID != "") {
            tempdata.push({
              resourceId: 2, title: element.WaitingArea.PatientName.trim(), start: strdate1 + "T" + element.WaitingArea.FromTime,
              end: strdate1 + "T" + element.WaitingArea.ToTime, appoinmentId: element.WaitingArea.AppointmentId, patientID: element.WaitingArea.PatientID, HeaderId: 2, email: element.WaitingArea.Email, phone: element.WaitingArea.Phoneno
              , dateOfBirth: element.WaitingArea.DateofBirth, gender: element.WaitingArea.Gender, address: element.WaitingArea.Address,patientName:element.WaitingArea.PatientName,doctorName:element.WaitingArea.DoctorName,
              serviceId:element.WaitingArea.ServiceID,note:element.WaitingArea.Note,reason:element.WaitingArea.Reason,positionID:element.WaitingArea.PositionID,reasonID:element.WaitingArea.ReasonID,appointmentFromTime:strdate1 + "T"+element.WaitingArea.AppointmentFromTime.trim(),appointmentToTime:strdate1 + "T"+element.WaitingArea.AppointmentToTime.trim(),roomNo:element.WaitingArea.ROOMNO


            });
          }
          else if (element.ConsultationRoom.DoctorID != "") {
            tempdata.push({
              resourceId: 3, title: element.ConsultationRoom.PatientName, start: strdate1 + "T" + element.ConsultationRoom.FromTime,
              end: strdate1 + "T" + element.ConsultationRoom.ToTime, appoinmentId: element.ConsultationRoom.AppointmentId, patientID: element.ConsultationRoom.PatientID, HeaderId: 3,
              email: element.ConsultationRoom.Email, phone: element.ConsultationRoom.Phoneno,patientName:element.ConsultationRoom.PatientName,doctorName:element.ConsultationRoom.DoctorName,
              dateOfBirth: element.ConsultationRoom.DateofBirth, gender: element.ConsultationRoom.Gender, address: element.ConsultationRoom.Address,roomNo:element.ConsultationRoom.ROOMNO,
              serviceId:element.ConsultationRoom.ServiceID,note:element.ConsultationRoom.Note,reason:element.ConsultationRoom.Reason,positionID:element.ConsultationRoom.PositionID,reasonID:element.ConsultationRoom.ReasonID,appointmentFromTime:strdate1 + "T"+element.ConsultationRoom.AppointmentFromTime.trim(),appointmentToTime:strdate1 + "T"+element.ConsultationRoom.AppointmentToTime.trim()

            });
          }
          else if (element.CheckingOut.DoctorID != "") {
            tempdata.push({
              resourceId: 4, title: element.CheckingOut.PatientName, start: strdate1 + "T" + element.CheckingOut.FromTime,
              end: strdate1 + "T" + element.CheckingOut.ToTime, appoinmentId: element.CheckingOut.AppointmentId, patientID: element.CheckingOut.PatientID, HeaderId: 4,
              email: element.CheckingOut.Email, phone: element.CheckingOut.Phoneno,patientName:element.CheckingOut.PatientName,doctorName:element.CheckingOut.DoctorName,
               dateOfBirth: element.CheckingOut.DateofBirth, gender: element.CheckingOut.Gender, address: element.CheckingOut.Address,
               serviceId:element.CheckingOut.ServiceID,note:element.CheckingOut.Note,reason:element.CheckingOut.Reason,positionID:element.CheckingOut.PositionID,reasonID:element.CheckingOut.ReasonID,appointmentFromTime:strdate1 + "T"+element.CheckingOut.AppointmentFromTime,appointmentToTime:strdate1 + "T"+element.CheckingOut.AppointmentToTime.trim(),roomNo:element.CheckingOut.ROOMNO

            });
          }
        });
        this.calendarEvents = tempdata;
      },
        err => {
          console.log(err);
        });
  }

  ngOnInit() {
    let today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    let strdate = mm + '' + dd + '' + yyyy;
    this.currentDate = mm + '' + dd + yyyy;
    let header = [];

    header.push({ id: 1, title: "Scheduled Appointments" });
    header.push({ id: 2, title: "Waiting Area" });
    header.push({ id: 3, title: "Consultation Room" });
    header.push({ id: 4, title: "Checking out" });

    this.calendarresources = header;

    this.getAppointmentDetails(strdate);
  }
  hideTimeCol() {
    this.calendarComponent.element.nativeElement.getElementsByClassName("fc-axis fc-widget-header")[0].style.display = 'none';
    let timecell = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-axis fc-time");
   
    for (let i = 0; i < timecell.length; i++) {
      timecell[i].style.display = 'none';
      //timecell1[i].style.display = 'none';
    }
    //this.calendarComponent.element.nativeElement.getElementsByClassName("fc-time")[0].style.display = 'none';
  }
  ngAfterViewInit() {
    this.hideTimeCol();
    this.bindEvents();
  }
  dragdate(arg: any) {
    this.roomNumber = "0";
    this.appoinmentId = arg.oldEvent._def.extendedProps.appoinmentId;
    if (parseInt(arg.newResource._resource.id) == 1) {
      this.FlowArea = "Appointment";
    }
    else if (parseInt(arg.newResource._resource.id) == 2) {
      this.FlowArea = "Waiting";
    }
    else if (parseInt(arg.newResource._resource.id) == 3) {
      this.FlowArea = "Encounter";
    }
    else if (parseInt(arg.newResource._resource.id) == 4) {
      this.FlowArea = "Finish";

    }

    if ((parseInt(arg.oldResource._resource.id)) == 1) {
      if (arg.oldEvent._def.extendedProps.IsReady == "True") {
          if (parseInt(arg.newResource._resource.id) == 3) {
            this.roomNumber=arg.oldEvent._def.extendedProps.roomNo;
            this.openRoomDialog('modaladdRoom');
          }
          else {
            this.updatePatientDetail("0");
            //arg.revert();
          }
      }
      else {
        arg.revert();
      }
    }
   // else if ((parseInt(arg.oldResource._resource.id)) == 2 && parseInt(arg.newResource._resource.id)<(parseInt(arg.oldResource._resource.id))) {
     else if(parseInt(arg.newResource._resource.id) == 3)
     {
      this.roomNumber=arg.oldEvent._def.extendedProps.roomNo;
      this.openRoomDialog('modaladdRoom');
    }
    else {
    //  arg.revert();
    this.updatePatientDetail("0");
    }

  }
  clickCount: number = 0;
  singleClickTimer: any
  rowclick(arg: any) {
    this.appoinmentId = arg.event.extendedProps.appoinmentId;
    let HeaderId=arg.event.extendedProps.HeaderId;
    this.clickCount++;
    let self = this;
    if (this.clickCount === 1) {
      this.singleClickTimer = setTimeout(function () {
        console.log('single click');
        if (HeaderId == 1) {
          self.markStatus = arg.event.extendedProps.IsReady == "True" ? true : false;
          self.openMarkIn("modalmarkInPatient")
        }
        self.clickCount = 0;
      }, 400);
    } else if (this.clickCount === 2) {
      console.log('double click');
      this.clickCount = 0;
      clearTimeout(this.singleClickTimer);
      this.clickCount++;
    
      let startdate = new Date(arg.event.extendedProps.appointmentFromTime);
      var dd = String(startdate.getDate()).padStart(2, '0');
      var mm = String(startdate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = startdate.getFullYear();
      //let strdate = dd+'/'+mm+'/'+yyyy; //mm + '' + dd + '' + yyyy;
      let strdate = mm + '/' + dd + '/' + yyyy; //mm + '' + dd + '' + yyyy;
      let starttime = String(startdate.getHours()) + ":" + String(startdate.getMinutes());

      let enddate = new Date(arg.event.extendedProps.appointmentToTime);
      dd = String(enddate.getDate()).padStart(2, '0');
      mm = String(enddate.getMonth() + 1).padStart(2, '0'); //January is 0!
      yyyy = enddate.getFullYear();
      let enddt = mm + '' + dd + '' + yyyy;
      let endtime = String(enddate.getHours()) + ":" + String(enddate.getMinutes());
      let temp = arg.el.innerText;
      //let appointmentId = "0";
      let patientName = "";
      patientName=temp;
      this._appointmentService.setBookingInfo("doctorBookingInfo", {
        "doctorid": arg.event.extendedProps.DoctorID,
        "doctorname": arg.event.extendedProps.doctorName, "startdate": strdate, "enddate": enddt, "starttime": starttime,
        "endtime": endtime, "patientname": patientName,
        "patientId": (arg.event.extendedProps != undefined ) ? arg.event.extendedProps.patientID : "",
        "appointmentid": this.appoinmentId, "email": (arg.event.extendedProps!= undefined ) ? arg.event.extendedProps.email : "",
        "phone": (arg.event.extendedProps.phone != undefined ) ? arg.event.extendedProps.phone : "",
        "dateOfBirth": (arg.event.extendedProps.dateOfBirth != undefined) ? arg.event.extendedProps.dateOfBirth : "",
        "gender": (arg.event.extendedProps != undefined ) ? arg.event.extendedProps.gender : "",
        "address": (arg.event.extendedProps != undefined ) ? arg.event.extendedProps.address: "",
        "serviceID":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.serviceId:"",
        "note":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.note:"",
        "reason":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.reason:"",
        "reasonID":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.reasonId:"",
        "positionID":(arg.event.extendedProps!=undefined)? arg.event.extendedProps.positionID:"",
       
      });
    //  alert("doctorBookingInfo=" + JSON.stringify(this._appointmentService.getBookingInfo("doctorBookingInfo")))
      this.router.navigate(['/flowsheet-book-appointment'],{queryParams:{id:1}});


    }
  }

  public openMarkIn(myModal: string) {
    this.error = false;
    document.getElementById(myModal).style.display = "block";
  }
  public closePopuop(myModal: string) {
    document.getElementById(myModal).style.display = "none";
    this.getAppointmentDetails(this.currentDate);
  }
  public handleError() {
    this.error = false;
  }

  public submitMarkIn(status: boolean) {
    if (status == undefined) {
      this.error = true;
      return false;
    }
    this._patientFlowSheetService.updateMarkStatus(this.appoinmentId, status).subscribe((res) => {
      if (res == 1) {
        console.log("success")
        this.getAppointmentDetails(this.currentDate);
      }
      else {
        console.log("Fail");
      }
    }, err => {
      console.log(err);
    })
    this.closePopuop('modalmarkInPatient');
  }
  public submitRoom(roomNo: string) {
    this.updatePatientDetail(roomNo);
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  public openRoomDialog(myModel: string) {
    this.error = false;
    document.getElementById(myModel).style.display = "block";
  }

  updatePatientDetail(roomNo: string) {
    this._patientFlowSheetService.updateAppoinmentDetail(this.appoinmentId, roomNo, this.FlowArea).subscribe((res) => {
      console.log("status=" + JSON.stringify(res))
      this.getAppointmentDetails(this.currentDate);
      this.closePopuop('modaladdRoom');
    }, err => {
      console.log(err);
    })
  }

  bindEvents() {
    let prevButton = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-prev-button");
    let nextButton = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-next-button");
    let todayButton = this.calendarComponent.element.nativeElement.getElementsByClassName("fc-today-button");

    nextButton[0].addEventListener('click', () => {
      this.hideTimeCol();
      let calendarApi = this.calendarComponent.getApi();
      let nextdaydate = new Date(calendarApi.getDate())
      var dd = String(nextdaydate.getDate()).padStart(2, '0');
      var mm = String(nextdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = nextdaydate.getFullYear();
      let strdate = mm + '' + dd + '' + yyyy;
      this.currentDate = strdate;
      this.getAppointmentDetails(strdate);
      console.log("nextClick")
    });
    prevButton[0].addEventListener('click', () => {
      this.hideTimeCol();
      let calendarApi = this.calendarComponent.getApi();
      let prevdaydate = new Date(calendarApi.getDate())
      var dd = String(prevdaydate.getDate()).padStart(2, '0');
      var mm = String(prevdaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = prevdaydate.getFullYear();
      let strdate = mm + '' + dd + '' + yyyy;
      this.currentDate = strdate;
      this.getAppointmentDetails(strdate);
      console.log("prevClick")
    });

    todayButton[0].addEventListener('click', () => {
      this.hideTimeCol();
      let calendarApi = this.calendarComponent.getApi();
      let todaydate = new Date(calendarApi.getDate())
      var dd = String(todaydate.getDate()).padStart(2, '0');
      var mm = String(todaydate.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = todaydate.getFullYear();
      let strdate = mm + '' + dd + '' + yyyy;
      this.currentDate = strdate;
      this.getAppointmentDetails(strdate);
      console.log("todayClick")
    });
  }
}
