import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientDetail, EmployerContact, EmergencyContact, Social, Billing, Insurance, Authorization } from './patient-model';

import { SharedService } from 'src/app/core/shared.service';
import { PatientCreateService } from '../services/patient-create.service'

@Component({
  selector: 'app-patient-create',
  templateUrl: './patient-create.component.html',
  styleUrls: ['./patient-create.component.css']
})
export class PatientCreateComponent implements OnInit {

  insuranceArray: Array<Insurance> = [];
  relationshipWithPatient=[];
  state=[];
  race=[];
  ethicity=[];
  preferredLanguage=[];
  preferredModeOfCommunication=[];
  providerName=[];
  provider_Name='';
  policy_Type='';
  status='';
  authorizationSelectedFile: File = null;
  patientId='0';
  ClsCreateUpdatePatient = {
    sPatientDetails: {},
    sPatientEmpContact: {},
    sPatientEmergency: {},
    sPatientSocial: {},
    sPatientBilling: {},
    sPatientInsurance: [],
    sPatientAuthorisation: {}
  }

  patient_FormGroup = new FormGroup({
    firstName: new FormControl(''),
    middleName:new FormControl(''),
    lastName: new FormControl(''),
    gender: new FormControl(''),
    DOB: new FormControl('')
  });

  patientContact_FormGroup = new FormGroup({
    contact_MobNo: new FormControl(''),
    contact_Phone: new FormControl(''),
    contact_Email: new FormControl(''),
    contact_AddressLine: new FormControl(''),
    contact_AddressLine1: new FormControl(''),
    contact_AddressCity: new FormControl(''),
    contact_AddressState: new FormControl(''),
    contact_AddressPostalCode: new FormControl(''),
    contact_PrimaryPhone: new FormControl(''),
    contact_SecondaryPhone: new FormControl('')
  });

  patientEmergency_FormGroup = new FormGroup({
    emergency_ContactName: new FormControl(''),
    emergency_ContactPhone: new FormControl(''),
    emergency_Relationship: new FormControl(''),
    employer_FullName: new FormControl(''),
    employer_Phone: new FormControl(''),
    employer_Address: new FormControl('')
  });

  patientSocial1_FormGroup = new FormGroup({
    social_MaritalStatus: new FormControl(''),
    social_MaritalStatusOther: new FormControl('')
  });

  patientSocial2_FormGroup = new FormGroup({
    social_GuardianFirstName: new FormControl(''),
    social_GuardianLastName: new FormControl(''),
    social_AddressLine: new FormControl(''),
    social_AddressCity: new FormControl(''),
    social_AddressState: new FormControl(''),
    social_AddressZip: new FormControl('')
  });

  patientSocial3_FormGroup = new FormGroup({
    social_DOB: new FormControl(''),
    social_GuardianSSN: new FormControl(''),
    social_PhoneNumber: new FormControl('')
  });

  patientSocial4_FormGroup = new FormGroup({
    social_PatientSSN: new FormControl(''),
    social_DriversLicenseFilePath: new FormControl(''),
    social_Race: new FormControl(''),
    social_Language: new FormControl(''),
    social_Ethicity: new FormControl(''),
    social_CommMode: new FormControl('')
  });

  patientBilling_FormGroup = new FormGroup({
    billingParty: new FormControl(''),
    billingPartyOther: new FormControl(''),
    billing_FirstName: new FormControl(''),
    billing_MiddleName: new FormControl(''),
    billing_LastName: new FormControl(''),
    billing_DOB: new FormControl(''),
    billing_AddressLine: new FormControl(''),
    billing_AddressLine1: new FormControl(''),
    billing_AddressCity: new FormControl(''),
    billing_AddressState: new FormControl(''),
    billing_AddressZip: new FormControl(''),
    billing_SSN: new FormControl(''),
    billing_DriversLicenseFilePath: new FormControl(''),
    billing_PrimaryPhone: new FormControl(''),
    billing_SecondaryPhone: new FormControl('')
  });

  patientInsurance_FormGroup = new FormGroup({
    insurance_ProviderName: new FormControl(''),
    insurancePolicy: new FormControl(''),
    insurance_PolicyType: new FormControl(''),
    insurance_CardImageFilePath: new FormControl(''),
    insurance_EffectiveFrom: new FormControl(''),
    insurance_Status: new FormControl('')
  });

  patientAuthorization_FormGroup = new FormGroup({
    authorizationFilePath: new FormControl('')
  });
  
  constructor(private patientCreateService: PatientCreateService, private _avRoute: ActivatedRoute, private router: Router, private sharedService: SharedService) { 
    
  }

  ngOnInit() {
    if(this._avRoute.snapshot.queryParams["pid"]){
      this.patientId= this._avRoute.snapshot.queryParams["pid"];
      this.getPatientInfoByPatientId(this.patientId);    
    }
    this.getMasterData('7');
    this.getMasterData('1');
    this.getMasterData('2');
    this.getMasterData('3');
    this.getMasterData('4');
    this.getMasterData('5');
    this.getMasterData('6');
    this.getMasterData('7');
  }

  onAuthorizationFileSelected(event){
    this.authorizationSelectedFile = event.target.files[0];
    const formData_Authorization= new FormData();
    formData_Authorization.append(this.authorizationSelectedFile.name, this.authorizationSelectedFile, this.authorizationSelectedFile.name);
    this.patientCreateService.fileUpload(formData_Authorization).subscribe(
      res => {
        console.log(res)
      },
      err => {console.log(err);}
    );
    console.log('this.insuranceSelectedFile:%o',this.authorizationSelectedFile);
  }

  addPolicy() {
    var policy_type_id = this.patientInsurance_FormGroup.value["insurance_PolicyType"];
    let data=undefined;
    if(policy_type_id ==='1'){
      data = this.insuranceArray.find(ob => ob['Policy_Type_Id'] === policy_type_id);
    }
    if (data === undefined) {
      var insurance: Insurance = {
        PatientId: this.patientId,
        Provider_Id: this.patientInsurance_FormGroup.value["insurance_ProviderName"],
        Provider_Name: this.provider_Name,
        Insurance_Policy: this.patientInsurance_FormGroup.value["insurancePolicy"],
        Policy_Type_Id: this.patientInsurance_FormGroup.value["insurance_PolicyType"],
        Policy_Type: this.policy_Type,
        Card_Image_FilePath: this.patientInsurance_FormGroup.value["insurance_CardImageFilePath"],
        Effective_From: this.patientInsurance_FormGroup.value["insurance_EffectiveFrom"],
        Status_Id: this.patientInsurance_FormGroup.value["insurance_Status"],
        Status: this.status,
      };
      this.insuranceArray.push(insurance);
    } else {
      alert("Primary insurance policy already exist.");
    }
  }
  providerChangeHandler(event){
    this.provider_Name = event.target.options[event.target.options.selectedIndex].text;
    // this.provider_Id=parseInt(event.target.value);
  }
  policyTypeChangeHandler(event){
    this.policy_Type = event.target.options[event.target.options.selectedIndex].text;
  }
  statusChangeHandler(event){
    this.status = event.target.options[event.target.options.selectedIndex].text;
  }

  finish() {
    this.ClsCreateUpdatePatient.sPatientDetails = this.getPatient();
    this.ClsCreateUpdatePatient.sPatientEmpContact = this.getEmployerContact();
    this.ClsCreateUpdatePatient.sPatientEmergency = this.getPatientEmergency();
    this.ClsCreateUpdatePatient.sPatientSocial = this.getPatientSocial();
    this.ClsCreateUpdatePatient.sPatientBilling = this.getPatientBilling();
    this.ClsCreateUpdatePatient.sPatientInsurance = this.getPatientInsurance();
    this.ClsCreateUpdatePatient.sPatientAuthorisation = this.getPatientAuthorization();

    this.patientCreateService.savePatient(this.ClsCreateUpdatePatient)
      .subscribe
      (
          res => {
            //console.log('Response:%o', res);
          var patientTemp = {
            PatientId: res,
            FirstName: this.patient_FormGroup.value["firstName"],
            MiddleName:this.patient_FormGroup.value["middleName"],
            LastName: this.patient_FormGroup.value["lastName"],
            DOB: this.patient_FormGroup.value["DOB"],
            Gender: this.patient_FormGroup.value["gender"]
          };
            this.sharedService.setLocalItem("patientDetail",patientTemp);
            this.router.navigate(['/patient-others']);
          }, 
          err => {console.log(err);}
      );
  }

  getPatient() {
    var patient: PatientDetail = {
      PatientId: this.patientId,
      FirstName: this.patient_FormGroup.value["firstName"],
      MiddleName:this.patient_FormGroup.value["middleName"],
      LastName: this.patient_FormGroup.value["lastName"],
      AddressLine: this.patientContact_FormGroup.value["contact_AddressLine"],
      AddressLine1: this.patientContact_FormGroup.value["contact_AddressLine1"],
      AddressCity: this.patientContact_FormGroup.value["contact_AddressCity"],
      AddressState: this.patientContact_FormGroup.value["contact_AddressState"],
      AddressPostalCode: this.patientContact_FormGroup.value["contact_AddressPostalCode"],
      AddressCountry: '',
      DOB: this.patient_FormGroup.value["DOB"],
      Gender: this.patient_FormGroup.value["gender"],
      Email: this.patientContact_FormGroup.value["contact_Email"],
      MobNo: this.patientContact_FormGroup.value["contact_PrimaryPhone"],
      PrimaryPhone: this.patientContact_FormGroup.value["contact_PrimaryPhone"],
      SecondaryPhone: this.patientContact_FormGroup.value["contact_SecondaryPhone"],
      ImageName: '',
      ImagePath: '',
      Flag: '',
      Age: '',
      RecopiaID: '',
      RecopiaName: ''
    };
    return patient;
  }

  getPatientEmergency() {
    var emergencyContact: EmergencyContact = {
      PatientId: this.patientId,
      ContactName: this.patientEmergency_FormGroup.value["emergency_ContactName"],
      ContactPhone: this.patientEmergency_FormGroup.value["emergency_ContactPhone"],
      Relationship: this.patientEmergency_FormGroup.value["emergency_Relationship"]
    };
    return emergencyContact;
  }

  getEmployerContact() {
    var employerContact: EmployerContact = {
      PatientId: this.patientId,
      Name: this.patientEmergency_FormGroup.value["employer_FullName"],
      Phone: this.patientEmergency_FormGroup.value["employer_Phone"],
      Address: this.patientEmergency_FormGroup.value["employer_Address"]
    };
    return employerContact;
  };

  getPatientSocial() {
    var social: Social = {
      PatientId: this.patientId,
      Marital_Status: this.patientSocial1_FormGroup.value["social_MaritalStatus"],
      Guardian_FName: this.patientSocial2_FormGroup.value["social_GuardianFirstName"],
      Guardian_LName: this.patientSocial2_FormGroup.value["social_GuardianLastName"],
      Add_Line: this.patientSocial2_FormGroup.value["social_AddressLine"],
      Add_City: this.patientSocial2_FormGroup.value["social_AddressCity"],
      Add_State: this.patientSocial2_FormGroup.value["social_AddressState"],
      Add_Zip: this.patientSocial2_FormGroup.value["social_AddressZip"],
      DOB: this.patientSocial3_FormGroup.value["social_DOB"],
      Patient_SSN: this.patientSocial4_FormGroup.value["social_PatientSSN"],
      Phone_Number: this.patientSocial3_FormGroup.value["social_PhoneNumber"],
      Guardian_SSN: this.patientSocial3_FormGroup.value["social_GuardianSSN"],
      Drivers_License_FilePath: this.patientSocial4_FormGroup.value["social_DriversLicenseFilePath"],
      Race: this.patientSocial4_FormGroup.value["social_Race"],
      Ethicity: this.patientSocial4_FormGroup.value["social_Ethicity"],
      Language: this.patientSocial4_FormGroup.value["social_Language"],
      Comm_Mode: this.patientSocial4_FormGroup.value["social_CommMode"],
    };
    return social;
  }
  getPatientBilling() {
    var billing: Billing = {
      PatientId: this.patientId,
      Billing_Party: this.patientBilling_FormGroup.value["billingParty"],
      First_Name: this.patientBilling_FormGroup.value["billing_FirstName"],
      Middle_Name: this.patientBilling_FormGroup.value["billing_MiddleName"],
      Last_Name: this.patientBilling_FormGroup.value["billing_LastName"],
      DOB: this.patientBilling_FormGroup.value["billing_DOB"],
      Add_Line: this.patientBilling_FormGroup.value["billing_AddressLine"],
      Add_Line1: this.patientBilling_FormGroup.value["billing_AddressLine1"],
      Add_City: this.patientBilling_FormGroup.value["billing_AddressCity"],
      Add_State: this.patientBilling_FormGroup.value["billing_AddressState"],
      Add_Zip: this.patientBilling_FormGroup.value["billing_AddressZip"],
      SSN: this.patientBilling_FormGroup.value["billing_SSN"],
      Drivers_License_FilePath: this.patientBilling_FormGroup.value["billing_DriversLicenseFilePath"],
      Primary_Phone: this.patientBilling_FormGroup.value["billing_PrimaryPhone"],
      Secondary_Phone: this.patientBilling_FormGroup.value["billing_SecondaryPhone"],
    };
    return billing;
  }

  getPatientInsurance() {
    return this.insuranceArray;
  }

  getPatientAuthorization() {
    var authorization: Authorization = {
      PatientId: this.patientId,
      Authorization_FilePath: this.patientAuthorization_FormGroup.value["authorizationFilePath"]
    };
    return authorization;
  }
  deleteRow(index){
    this.insuranceArray.splice(index,1);
  }

  getPatientInfoByPatientId(patientId){
    this.patientCreateService.getPatientInfoByPatientId(patientId)
    .subscribe
    (
        res => {
          //console.log('Response:%o', res);
          this.patientId = res.sPatientDetails.PatientId;
          this.setPatient(res.sPatientDetails);
          this.setPatientEmergency(res.sPatientEmergency);
          this.setEmployerContact(res.sPatientEmpContact);
          this.setPatientSocial(res.sPatientSocial);
          this.setPatientBilling(res.sPatientBilling);
          this.setPatientInsurance(res.sPatientInsurance);
        if (res.sPatientAuthorisation !== null) {
            this.setPatientAuthorization(res.sPatientAuthorisation);
          }
        }, 
        err => {console.log(err);}
    );
  }
  
  setPatient(sPatientDetails) {
    this.patientId =sPatientDetails.PatientId;
    this.patient_FormGroup.patchValue({
      firstName:sPatientDetails.FirstName,
      middleName:sPatientDetails.MiddleName,
      lastName:sPatientDetails.LastName,
      DOB:sPatientDetails.DOB,
      gender:sPatientDetails.Gender
    });
    this.patientContact_FormGroup.patchValue({
      contact_AddressLine:sPatientDetails.AddressLine,
      contact_AddressLine1:sPatientDetails.AddressLine1,
      contact_AddressCity:sPatientDetails.AddressCity,
      contact_AddressState:sPatientDetails.AddressState,
      contact_AddressPostalCode:sPatientDetails.AddressPostalCode,
      contact_Email:sPatientDetails.Email,
      contact_MobNo:sPatientDetails.MobNo,
      contact_PrimaryPhone:sPatientDetails.PrimaryPhone,
      contact_SecondaryPhone:sPatientDetails.SecondaryPhone
    });
  }

  setPatientEmergency(sPatientEmergency) {
    this.patientEmergency_FormGroup.patchValue({
      emergency_ContactName:sPatientEmergency.ContactName,
      emergency_ContactPhone:sPatientEmergency.ContactPhone,
      emergency_Relationship:sPatientEmergency.Relationship
    });
  }

  setEmployerContact(sPatientEmpContact) {
    this.patientEmergency_FormGroup.patchValue({
      employer_FullName:sPatientEmpContact.Name,
      employer_Phone:sPatientEmpContact.Phone,
      employer_Address:sPatientEmpContact.Address
    });
  };

  setPatientSocial(sPatientSocial) {
      this.patientSocial1_FormGroup.patchValue({
        social_MaritalStatus: sPatientSocial.Marital_Status
      });
      
      this.patientSocial2_FormGroup.patchValue({
        social_GuardianFirstName: sPatientSocial.Guardian_FName,
        //social_GuardianLastName:sPatientSocial.Guardian_LName,
        social_AddressLine: sPatientSocial.Add_Line,
        social_AddressCity: sPatientSocial.Add_City,
        social_AddressState: sPatientSocial.Add_State,
        social_AddressZip: sPatientSocial.Add_Zip
      });

    //try {
      this.patientSocial3_FormGroup.patchValue({
        social_DOB: sPatientSocial.DOB,
        social_PhoneNumber: sPatientSocial.Phone_Number,
        social_GuardianSSN: sPatientSocial.Guardian_SSN
      });
    //} catch (error) { console.log('error:%o', error); }    

    this.patientSocial4_FormGroup.patchValue({
      social_PatientSSN:sPatientSocial.Patient_SSN,
      //social_DriversLicenseFilePath:sPatientSocial.Drivers_License_FilePath,
      social_Race:sPatientSocial.Race,
      social_Ethicity:sPatientSocial.Ethicity,
      social_Language:sPatientSocial.Language,
      social_CommMode:sPatientSocial.Comm_Mode,
    });

  }
  setPatientBilling(sPatientBilling) {
    //try {
      this.patientBilling_FormGroup.patchValue({
        billingParty: sPatientBilling.Billing_Party,
        billing_FirstName: sPatientBilling.First_Name,
        billing_MiddleName: sPatientBilling.Middle_Name,
        billing_LastName: sPatientBilling.Last_Name,
        billing_DOB: sPatientBilling.DOB,
        billing_AddressLine: sPatientBilling.Add_Line,
        billing_AddressLine1: sPatientBilling.Add_Line1,
        billing_AddressCity: sPatientBilling.Add_City,
        billing_AddressState: sPatientBilling.Add_State,
        billing_AddressZip: sPatientBilling.Add_Zip,
        billing_SSN: sPatientBilling.SSN,
        //billing_DriversLicenseFilePath: sPatientBilling.Drivers_License_FilePath,
        billing_PrimaryPhone: sPatientBilling.Primary_Phone,
        billing_SecondaryPhone: sPatientBilling.Secondary_Phone
      });
    //} catch (error) { console.log('error:%o', error); }    
  }

  setPatientInsurance(sPatientInsurance) {
    sPatientInsurance.forEach(element => {
      var insurance: Insurance = {
        PatientId: element.PatientId,
        Provider_Name: element.Provider_Name,
        Provider_Id: element.Provider_Id,
        Insurance_Policy: element.Insurance_Policy,
        Policy_Type: element.Policy_Type,
        Policy_Type_Id: element.Policy_Type_Id,
        Card_Image_FilePath: element.Card_Image_FilePath,
        Effective_From: element.Effective_From,
        Status_Id: element.Status_Id,
        Status: element.Status,
      };
      this.insuranceArray.push(insurance);
    });
    return this.insuranceArray;
  }

  setPatientAuthorization(sPatientAuthorization) {
      this.patientAuthorization_FormGroup.patchValue({
        //authorizationFilePath: sPatientAuthorization.AuthorizationFilePath
      });
  }

  getMasterData(key){
    this.patientCreateService.getMasterData(key)
    .subscribe
    (
        res => {
          //console.log('Response:%o', res);
          switch (key) {  
            case '1':  
              this.relationshipWithPatient = res; 
             break;  
            case '2':  
            this.state = res;  
             break;  
            case '3':  
            this.race = res;   
             break;  
            case '4':  
            this.ethicity = res;   
             break;  
            case '5':  
            this.preferredLanguage = res;  
             break;  
             case '6':  
             this.preferredModeOfCommunication = res; 
             break;  
             case '7':  
             this.providerName = res;   
             break;  
           } 
        }, 
        err => {console.log(err);}
    );
  }
}
