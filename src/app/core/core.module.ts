import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonService } from './common.service';
import { SharedService } from './shared.service';
import { httpInterceptorProviders } from './http-interceptor';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    CommonService,
    SharedService
  ]
})
export class CoreModule { }
