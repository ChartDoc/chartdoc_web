import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SharedService } from '../core/shared.service';
import { PatientImpressionPlanComponent } from '../patient-profile/patient-impression-plan/patient-impression-plan.component';

const localBASE_URL = 'api/ChartDoc/';
const epiclBASE_URL = 'api/ChartDoc';

@Injectable({
  providedIn: 'root'
})
export class PatientImpressionPlanService {

  constructor(private _http: HttpClient,
              private sharedService: SharedService) { }

  public getPatientImpressionplan(patientId: string, flag: string): Observable<any> {

    const endpoint = 'GetImpressionPlan/';

    const myHeaders = new HttpHeaders();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('Accept', 'text/plain');

    if (flag === 'L') {
      patientId  = btoa(patientId);
      return this._http.get(environment.baseUrl + localBASE_URL + endpoint + `${patientId}`, { headers: myHeaders});
    } else { // require to use epic url
      patientId  = btoa(patientId);
      return this._http.get(environment.baseUrl + localBASE_URL + endpoint + `${patientId}`, { headers: myHeaders});
    }
  }

  setImpressionPlanValue(key: string, val: string) {
    this.sharedService.setLocalItem(key, val);
  }

  getImpressionPlanValue(key: string): string {
    return this.sharedService.getLocalItem(key);
  }

  getPatientDetails(patientInfo: string) {
    return this.sharedService.getLocalItem(patientInfo);
  }

  saveImpressionPlan(impressionPlanInfo: any): Observable<any> {
    console.log('HI Save Method from service');
    return this._http.post(environment.baseUrl + localBASE_URL + 'SaveImpressionPlan',impressionPlanInfo);

  }


}
