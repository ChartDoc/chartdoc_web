import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { User } from '../models/user.model';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective } from '@angular/forms';
import { EventInput } from '@fullcalendar/core';
import { DatePipe } from '@angular/common';
import { AcceptcopayService } from '../services/accept-copay.service';
import { OfficeCalendarService } from '../services/officecalendar.service';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
    selector: 'app-accept-copay',
    templateUrl: './accept-copay.component.html',
    styleUrls: ['./accept-copay.component.css']
})
export class AcceptcopayComponent implements OnInit {
    formData = new FormData();
    @ViewChild('formDir', { static: false }) private formDirective: NgForm;
    acceptcopayForm: FormGroup;
    doctors: any;
    calendardtl: any;
    today = new Date();
    minDate = undefined;
    patientId = 0;
    paymentTypes: any
    constructor(private router: Router,
        private loginService: AuthenticationService,
        private formBuilder: FormBuilder,
        private _avRoute: ActivatedRoute,
        private _acceptcopayService: AcceptcopayService,
        private config: NgbDatepickerConfig,
        public toastr: ToastrManager) {

        const current = new Date();
        // this.minDate = {
        //     year: current.getFullYear(),
        //     month: current.getMonth() + 1,
        //     day: current.getDate()
        // };
        this.minDate = current;
        if (this._avRoute.snapshot.queryParams["pid"]) {

            this.patientId = this._avRoute.snapshot.queryParams["pid"];

        }
        this.acceptcopayForm = this.formBuilder.group({
            id: ['0'],
            paymenttype: ['0', [Validators.required]],
            refNo1: ['', [Validators.required]],
            refNo2: [''],
            paymentdate: [''],
            paydate: ['', [Validators.required]],
            amount: ['0', [Validators.required]],
            patientId: [this.patientId],
            add_line: [''],

        });

    }
    ngAfterViewInit() {


    }
    ngOnInit() {
        this.getPaymenttype(9);
    }
    getPaymenttype(id: any) {
        let self = this;
        this._acceptcopayService.getPaymentType(id)
            .subscribe((res) => {
                this.paymentTypes = res;
            })
    };
    ngOnDestroy() {
        //document.body.className = "sidebar-collapse";

    }
    save() {
        if (!this.acceptcopayForm.valid) {
            return;
        } else {
            this.acceptcopayForm.patchValue({
                paymentdate: String(this.acceptcopayForm.value.paydate.getMonth() + 1).padStart(2, '0') + "" +
                  String(this.acceptcopayForm.value.paydate.getDate()).padStart(2, '0') + "" +
                  this.acceptcopayForm.value.paydate.getFullYear()
            });
            this._acceptcopayService.saveCopay(this.acceptcopayForm.value)
                .subscribe((res) => {
                    console.log('Response:%o', res);
                    this.toastr.successToastr(" Save sucessfully..!", 'Success!');
                    this.router.navigate(['/flowsheet-book-appointment'],{queryParams:{id:1}});
                }, err => {
                    console.log(err);
                    this.toastr.errorToastr(String(err) + " , please contact system admin!", 'Oops!');
                   // this.acceptcopayForm.value.fromTime = fromtime;
                   // this.acceptcopayForm.value.fromTime = toTime;
                });
        }
    }
    reset(): void {
        this.acceptcopayForm.reset();
        this.acceptcopayForm.patchValue({
            
            id: 0
        })
    }



}
