import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommonService } from 'src/app/core/common.service';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';
import { PatientFollowupService } from '../../services/patient-followup.service';


@Component({
  selector: 'app-patient-follow-up',
  templateUrl: './patient-follow-up.component.html',
  styleUrls: ['./patient-follow-up.component.css']
})
export class PatientFollowUpComponent implements OnInit, OnDestroy {

  OneDayImageName = 'btn-day.png';
  OneWeekImageName = 'btn-week.png';
  OneMonthImageName = 'btn-month.png';
  ThreeMonthImageName = 'btn-month-3.png';
  SixMonthImageName = 'btn-month-6.png';
  OneYearImageName = 'btn-year.png';
  ScheduleImageName = 'btn-scheduleD.png';

  pipe = new DatePipe('en-US'); // Use your own locale

  FollowUpDate: string;
  FollowUpDateImage: string;
  LatestFollowup: string;
  message: string;

  _formFollowup: FormGroup;

  PatientId: string;

  Commonsubscription: Subscription;
  Apisubscription: Subscription;

  Flag: string;
  SaveFollupTemp: string;
  patientInfo: any;


  constructor(private FollowupService: PatientFollowupService, private commonService: CommonService) { }

  ngOnDestroy() {
    // this.Commonsubscription.unsubscribe();
    // this.Apisubscription.unsubscribe();
  }

  ngOnInit() {
    this.patientInfo = this.FollowupService.getPatientDetails('patientInfo');
    this.PatientId = this.patientInfo.patientId;
    this.Flag = this.patientInfo.flag;

    this.FollowUpDate = '';
    this._formFollowup = new FormGroup({
      pFollowupDate: new FormControl(''),
      pFollowupPeriod: new FormControl(''),
      PatientId: new FormControl('')
    });

    //this.LatestFollowup = this.FollowupService.getLatestFollowUp('followUp' + this.PatientId);
    this.Commonsubscription = this.commonService.CurrentFollowUpLatest.subscribe
        (
          data => {
            this.LatestFollowup = data;
          }
        );

    if (this.LatestFollowup === '') {
          this.Apisubscription = this.FollowupService.GetPatientFollowup(this.PatientId, this.Flag)
            .subscribe((res) => {
              if (res[0].pFollowupPeriod != '') {
                this.FollowupDay(res[0].pFollowupPeriod);
              } else if (res[0].pFollowupDate != '') {
                this.FollowupSelectDate((res[0].pFollowupDate));
              }
            },
            err => {
              console.log(err);
            });
          }

    if (this.LatestFollowup == 'SD') {
          this.Commonsubscription = this.commonService.CurrentFollowUpDate.subscribe
            (
              data => {
                this.FollowUpDate = data;
                this._formFollowup.get('pFollowupDate').setValue(data);
              }
            );
        } else {
          this.Commonsubscription = this.commonService.CurrentFollowupDay.subscribe
            (
              data => {
                this.FollowUpDate = data;
                this._formFollowup.get('pFollowupDate').setValue(data);
              }
            );
        }


    if (this.LatestFollowup == 'SD') {
        this.Commonsubscription = this.commonService.CurrentFollowUpDateImage.subscribe
          (
              data => {
                if (data) {
                  this.FollowUpDateImage = data;
                  this.ScheduleImageName = this.FollowUpDateImage;
                }
              }
            );
        } else {
          this.Commonsubscription = this.commonService.CurrentFollowupDayImage.subscribe
          (
            data => {
              if (data) {
              this.ResetUnselectedView();
              this.FollowUpDateImage = data;
              if (this.LatestFollowup == '1Day') {
                this.OneDayImageName = this.FollowUpDateImage;
                }
              if (this.LatestFollowup == '1Week') {
                this.OneWeekImageName = this.FollowUpDateImage;
                }
              if (this.LatestFollowup == '1Month') {
                this.OneMonthImageName = this.FollowUpDateImage;
                }
              if (this.LatestFollowup == '3Month') {
                this.ThreeMonthImageName = this.FollowUpDateImage;
                }
              if (this.LatestFollowup == '6Month') {
                this.SixMonthImageName = this.FollowUpDateImage;
                }
              if (this.LatestFollowup == '1Year') {
                this.OneYearImageName = this.FollowUpDateImage;
                }
              }
            }
          );
        }
  }

  FollowupDay(value: string) {
    this.LatestFollowup = value;
    this.UpdateFollowUpDayView(value);
  }

  UpdateFollowUpDayView(value: string) {
    this.ResetUnselectedView();
    if (value == '1Day') {
      this.FollowUpDate = '1Day';
      this.OneDayImageName = 'btn-day-hover.png';

      this.commonService.FollowUp('1Day');
      this.commonService.FollowupDay('1 Day');
      this.commonService.FollowupDayImage('btn-day-hover.png');
      this.SetDateLabelVal('1Day');
    } else
    if (value == '1Week') {
      this.FollowUpDate = '1Week';
      this.OneWeekImageName = 'btn-week-hover.png';

      // console.log(this.OneWeekImageName);

      this.commonService.FollowUp('1Week');
      this.commonService.FollowupDay('1 Week');
      this.commonService.FollowupDayImage('btn-week-hover.png');
      this.SetDateLabelVal('1Week');

    } else if (value == '1Month') {
      this.FollowUpDate = '1Month';
      this.OneMonthImageName = 'btn-month-hover.png';

      this.commonService.FollowUp('1Month');
      this.commonService.FollowupDay('1 Month');
      this.commonService.FollowupDayImage('btn-month-hover.png');
      this.SetDateLabelVal('1Month');
    } else if (value == '3Month') {
      this.FollowUpDate = '3Month';
      this.ThreeMonthImageName = 'btn-month-3-hover.png';

      this.commonService.FollowUp('3Month');
      this.commonService.FollowupDay('3 Months');
      this.commonService.FollowupDayImage('btn-month-3-hover.png');
      this.SetDateLabelVal('3Month');
    } else if (value == '6Month') {
      this.FollowUpDate = '6Month';
      this.SixMonthImageName = 'btn-month-6-hover.png';

      this.commonService.FollowUp('6Month');
      this.commonService.FollowupDay('6 Months');
      this.commonService.FollowupDayImage('btn-month-6-hover.png');
      this.SetDateLabelVal('6Month');
    } else if (value == '1Year') {
      this.FollowUpDate = '1Year';
      this.OneYearImageName = 'btn-year-hover.png';

      this.commonService.FollowUp('1Year');
      this.commonService.FollowupDay('1 Year');
      this.commonService.FollowupDayImage('btn-year-hover.png');
      this.SetDateLabelVal('1Year');
    }
  }

  FollowupSelectDate(value: string): void {
    this.ResetUnselectedView();

    this.LatestFollowup = 'SD';
    this.commonService.FollowUp('SD');
    this.commonService.FollowupDate(this.pipe.transform(new Date(value), 'MMM dd,yyyy'));
    this.commonService.FollowupDateImage('btn-scheduleD-hover.png');
    this.FollowUpDate = this.pipe.transform(new Date(value), 'MMM dd,yyyy');
    this.SetDateLabelVal(this.pipe.transform(new Date(value), 'MMM dd,yyyy'));

    this.ScheduleImageName = 'btn-scheduleD-hover.png';
  }

  SetDateLabelVal(value: string) {
    this._formFollowup.get('pFollowupDate').setValue(value);
  }

  ResetUnselectedView() {
    this.OneDayImageName = 'btn-day.png';
    this.OneWeekImageName = 'btn-week.png';
    this.OneMonthImageName = 'btn-month.png';
    this.ThreeMonthImageName = 'btn-month-3.png';
    this.SixMonthImageName = 'btn-month-6.png';
    this.OneYearImageName = 'btn-year.png';
    this.ScheduleImageName = 'btn-scheduleD.png';
  }

  public saveFollowUp() {

    if (this.LatestFollowup === 'SD') {
      this._formFollowup.setValue({
        pFollowupDate: this.FollowUpDate,
        pFollowupPeriod: '',
        PatientId: this.PatientId
       });
    } else {
      this.FollowUpDate = this.FollowUpDate.replace(' ','');
      this._formFollowup.setValue({
        pFollowupDate: '',
        pFollowupPeriod: this.FollowUpDate,
        PatientId: this.PatientId
      });
     }

    this.FollowupService.saveFolloUp(this._formFollowup.value).subscribe
     (
       data => {
         if (data > 0) {
           this.message = 'Operation Successful.';
         } else {
          this.message = 'Operation Unsuccessful.';
         }
       }
     );
   }


}
