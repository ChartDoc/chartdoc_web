import { Component, OnInit } from '@angular/core';
import { PatientAllergiesService } from '../../services/patient-allergies.service';
import { Allergies } from '../../models/patient-allergies.model';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-patient-allergies',
  templateUrl: './patient-allergies.component.html',
  styleUrls: ['./patient-allergies.component.css']
})
export class PatientAllergiesComponent implements OnInit {
  patientInfo: any;
  public allergiesreaction: string;
  public allergiesList: any;
  allergy: Allergies[] = [];
  allergyCode: string;
  allergyDisplay: string;
  isRecordFound: boolean;
  public allergydesc: string;
  entry: any;
  resource: any;
  patientId: string;
  flag: string;
  constructor(private _Allergies: PatientAllergiesService) { }

  ngOnInit() {
    this.patientInfo = this._Allergies.getPatientDetails('patientInfo');
    this.flag = this.patientInfo.flag;
    this.patientId = this.patientInfo.patientId;
    
    if(isNullOrUndefined(this._Allergies.getAllergiesInfo('allergies' + this.patientId))){
      this._Allergies.getAllergies(this.patientId, this.flag)
      .subscribe((res) => {
        if (this.flag === 'E') {
          this.allergiesList = res.entry;
          if (this.allergiesList.length > 0) {
            this.isRecordFound = true;
            this.allergiesList.forEach(allergyTollerance => {
              allergyTollerance.resource.reaction.forEach(reaction => {
                reaction.manifestation.forEach(element => {
                  this.allergy.push({AllergiesDesc: element.text});
                });
              });
            });
          }
        } else {
          this.allergiesList = res;
          this.allergiesList.forEach(element => {
            this.allergy.push({AllergiesDesc: element.Description});
          });
        }
        this._Allergies.setAllergiesInfo('allergies' + this.patientId, this.allergy);
      },
      err => {
        console.log(err);
      });
    }
    else{
      this._Allergies.getAllergiesInfo('allergies' + this.patientId)
    }
  }
}

