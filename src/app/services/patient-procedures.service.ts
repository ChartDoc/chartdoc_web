import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from '../core/shared.service';
import { environment } from 'src/environments/environment';

// const BASE_URL="api/FHIR/DSTU2";

@Injectable({
  providedIn: 'root'
})
export class PatientProceduresService {

  constructor(private _http: HttpClient, private sharedService: SharedService) { }

  public GetPatientProcedures(patientId: string, flag: string): Observable<any>{

    //patientId = 'Tbt3KuCY0B5PSrJvCu2j-PlK.aiHsu2xUjUM8bWpetXoB'; //Required to comment out

    const epicEndpoint = 'api/FHIR/DSTU2/Procedure';
    const baseEndpoint = 'api/ChartDoc/GetProceduresByPatientId';
   
    let myHeaders = new HttpHeaders(); 
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('Accept', 'text/plain');  

    // let myParams = new HttpParams(); 
    // myParams.append('PatientID', id);
    if(flag == 'E'){
      return this._http.get(environment.epicApiUrl + epicEndpoint, {params: new HttpParams().set('patient',patientId), headers: myHeaders});
    }else{
      patientId=btoa(patientId);
      return this._http.get(environment.baseUrl + baseEndpoint + `/${patientId}`, {headers: myHeaders});
    }
    
  }

  getPatientDetails(patientInfo: string){
    return this.sharedService.getLocalItem(patientInfo);
  }
}
