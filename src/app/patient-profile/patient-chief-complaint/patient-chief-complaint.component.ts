import { Component, OnInit,OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import { PatientChiefComplaintService } from '../../services/patient-chief-complaint.service';
import { FormGroup, FormControl } from '@angular/forms';
import { isNullOrEmptyString } from '@progress/kendo-angular-grid/dist/es2015/utils';

@Component({
  selector: 'app-patient-chief-complaint',
  templateUrl: './patient-chief-complaint.component.html',
  styleUrls: ['./patient-chief-complaint.component.css']
})
export class PatientChiefComplaintComponent implements OnInit {

  public Editor = DecoupledEditor;
  public ckeditorContent: string;
  tempckeditorContent: string;
  dataDefault:any;
  PatientId:string;
  Commonsubscription: Subscription;
  Apisubscription: Subscription;
  ccValue: string;
  patientInfo: any;
  formCC:FormGroup;
  Flag:string;

  constructor(private CCService:PatientChiefComplaintService) { 
  }
  
  ngOnDestroy(){
  }

  ngOnInit() {
    
    
    this.formCC = new FormGroup({
      pCcDescription: new FormControl("")  ,
      PId:new FormControl("")
    });  

    
    this.patientInfo = this.CCService.getPatientDetails('patientInfo');
    this.Flag = this.patientInfo.flag;
    this.PatientId = this.patientInfo.patientId;

    this.tempckeditorContent = this.CCService.getChiefComplaintValue('cc'+this.PatientId);

    if(isNullOrEmptyString(this.tempckeditorContent)){
      this.Apisubscription = this.CCService.GetPatientCC(this.PatientId,this.Flag)
      .subscribe((res) => {
        this.ckeditorContent = res[0].pCcDescription;
        this.CCService.setChiefComplaintValue('cc'+this.PatientId, this.ckeditorContent);
      },
      err => {  
        console.log(err);
      });
    }
    else{
      this.ckeditorContent = this.tempckeditorContent;
    }
}

  public onReady( editor:any ) {
    editor.ui.getEditableElement().parentElement.insertBefore(
        editor.ui.view.toolbar.element,
        editor.ui.getEditableElement(),
    );

    
  }
  public onChange( { editor }: ChangeEvent ) {
    const data = editor.getData();
    this.CCService.setChiefComplaintValue('cc'+this.PatientId, data);
  }

  public saveCC(){
    this.formCC.setValue({
      pCcDescription: this.ckeditorContent,
      PId: this.PatientId
    })

    this.CCService.saveCC(this.formCC.value).subscribe
    (
      data=>
      {
        // this.CCService.removeLocalCC('cc'+this.PatientId);
        // this.ckeditorContent = "";
      }      
    ) 
  }
}