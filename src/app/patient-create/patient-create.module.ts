import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientCreateComponent } from './patient-create.component';
import { CoreModule } from '../core/core.module';
import { PatientCreateRoutingModule } from './patient-create-routing.module';
import { ShellModule } from '../shell/shell.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [PatientCreateComponent],
  imports: [
    CommonModule,
    CoreModule,
    PatientCreateRoutingModule,
    ShellModule,
    ReactiveFormsModule,
  ],
  exports: [PatientCreateComponent]
})
export class PatientCreateModule { }
