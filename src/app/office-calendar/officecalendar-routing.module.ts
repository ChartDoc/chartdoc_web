import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OfficeCalendarComponent } from './officecalendar.component';


const officecalendarRoutes: Routes = [
    {path:'office-calendar', component: OfficeCalendarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(officecalendarRoutes)],
  exports: [RouterModule]
})
export class OfficeCalendarRoutingModule { }
